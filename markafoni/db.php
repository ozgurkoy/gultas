<?php
abstract class AbsConnection {

	abstract function connect();
	abstract function getConnection();
	abstract function setConnection($conn);
}

class Connection extends AbsConnection {

	private $connection;
	private $username;
	private $password;
	private $dsn;

	/**
	 * To connect to another database, change the "mysql" in the dsn attribute to
	 * the database you want.
	 * for example: pgsql:dbname...
	 */
	public function __construct() {
		$this->dsn = "mysql:dbname=office;host=localhost;charset=utf8;";
		$this->username = "dev0";
		//$this->dsn = "mysql:dbname=admcomft_ajdapekkan;host=localhost;charset=utf8;";
		//$this->username = "admcomft_ap";
		$this->password = "Kow8U697mAv";
	}

	public function setConnection($conn) {
		$this->connection = $conn;
	}

	public function getConnection() {
		return $this->connection;
	}
	/**
	 * Make a connection with a database using PDO Object.
	 *
	 */
	public function connect() {
		try {
			$pdoConnect = new PDO($this->dsn, $this->username, $this->password);
			$this->connection = $pdoConnect;
		} catch (PDOException $e) {
			die("Error connecting database: Connection::connect(): ".$e->getMessage());
		}
	}
	/**
	 * Execute a DML
	 *
	 * @param String $query
	 */
	public function executeDML($query) {
		if (!$this->getConnection()->query($query)) {
			throw new Error($this->getConnection()->errorInfo());
		} else {
			return true;
		}
	}
	/**
	 * Execute a query
	 *
	 * @param String $query
	 * @return PDO ResultSet Object
	 */
	public function executeQuery($query) {
		$rs = null;
		if ($stmt = $this->getConnection()->prepare($query)) {
			if ($this->executePreparedStatement($stmt, $rs)) {
				return $rs;
			}
		} else {
			throw new Error($this->getConnection()->errorInfo());
		}
	}

	/**
	 * Execute a prepared statement
	 * it is used in executeQuery method
	 *
	 * @param PDOStatement Object $stmt
	 * @param Array $row
	 * @return boolean
	 */
	private function executePreparedStatement($stmt, & $row = null) {
		$boReturn = false;
		if ($stmt->execute()) {
			if ($row = $stmt->fetchAll()) {
				$boReturn = true;
			} else {
				$boReturn = false;
			}
		} else {
			$boReturn = false;
		}
		return $boReturn;
	}

	/**
	 * Init a PDO Transaction
	 */
	public function beginTransaction() {
		if (!$this->getConnection()->beginTransaction()) {
			throw new Error($this->getConnection()->errorInfo());
		}
	}
	/**
	 * Commit a transaction
	 *
	 */
	public function commit() {
		if (!$this->getConnection()->commit()) {
			throw new Error($this->getConnection()->errorInfo());
		}
	}
	/**
	 * Rollback a transaction
	 *
	 */
	public function rollback() {
		if (!$this->getConnection()->rollback()) {
			throw new Error($this->getConnection()->errorInfo());
		}
	}
}

class Error extends Exception {

	private $codSQLState;
	private $codDriverError;
	private $msgError;
	private $isOk;

	/**
	 * Verify if exists a error.
	 *
	 * @param array $arError
	 */
	public function __construct($arError=null) {

		if (!is_null($arError)) {
			if (count($arError)==3) {
				$this->errorOcurred($arError);
			}
		}
	}

	public function getCodSQLState() {
		return $this->codSQLState;
	}
	public function setCodSQLState($codSqlState) {
		$this->codSQLState = $codSqlState;
	}
	public function getCodDriverError() {
		return $this->codDriverError;
	}
	public function setCodDriverError($codDriverError) {
		$this->codDriverError = $codDriverError;
	}
	public function getMsgError() {
		return $this->msgError;
	}
	public function setMsgError($msgError) {
		$this->msgError = $msgError;
	}

	/**
	 * If a error occurs the object will be informed
	 *
	 * @param array $arErrors
	 */
	public function errorOcurred($arErrors) {
		$this->setCodSQLState($arErrors[0]);
		$this->setCodDriverError($arErrors[1]);
		$this->setMsgError($arErrors[2]);
	}

}
?>
