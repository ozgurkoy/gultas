<?php
class CrmPublisher extends Crm
{
	function allPublishers()
	{
		$this->hasPermission($this->get("PermissionTypes.publishers"));

		$this->set('pagetitle', 'Publishers');
		$this->set('content', 'allpublishers');

		$sql = "
			SELECT
				u.avatar,
				u.username,
				u.name,
				u.twitterid,
				u.type,
				n.follower,
				n.influence,
				n.adPrice AS cost,
				n.followerUpdateDate,
				n.costUpdateDate,
                                u.groupId,
				ut.type AS typeName
			FROM users u
				LEFT JOIN numbers n ON n.userID = u.twitterid
				LEFT JOIN userType ut ON u.type = ut.ID
		";

		$AllGroups = new Axon("groups");

		switch ($this->get("PARAMS.tid")) {
			case "0":
				break;
			case "NULL":
				$sql .= " WHERE u.groupId IS NULL";
				$this->set('content', 'allpublishersedit');
				$this->set("AllGroups", $AllGroups->afind());
				break;
			default:
				$sql .= " WHERE u.type=" . intval($this->get("PARAMS.tid"));
				break;
		}

		DB::sql($sql);

		$publishers = $this->get("DB")->result;

		$this->set("publishers", $publishers);
		$this->set("userTypes", $this->getUserTypes());
	}

	function PublisherPreview()
	{

		$this->set('pagetitle', 'Publisher Preview');
		$this->set('content', 'publisherReview');

		$u = $this->get("GET.u");

		//$getDataResults1 = CrmCommon::GetTwitterData($u);
		$data = CrmCommon::GetFromCalcAPI("check",$u);

		$data = $data->responseBody->tdata;
		//die(print_r($data,true));

		// Check if user Exist
		if (!is_object($data) || !isset($data->id)) {
			die(json_encode(array("error" => 1, "description" => "User Name Not Found ", "username" => $u)));
		}

		// Load User
		$userData = CrmCommon::SearchPublishers($data->id);

		if ($userData != NULL) {
			$this->set('userData', $userData);
			$this->set('Groups', CrmCommon::GetAllUserGroups());
			$this->set('PublisherTags', CrmCommon::GetAllTags());


			$userTags = new Axon("UsersTags");

			$this->set('UserTags', CrmCommon::GetAllPublisherTags($userData[0]['ID']));
			$this->set('adMinglePublisher', true);
		} else {
			$this->set('adMinglePublisher', false);
		}


		$this->set('twitterData', $data);


		//$this->set('twitterDataJson', json_encode($obj));

		$sql = "
			SELECT
				DATE(FROM_UNIXTIME(nh.costUpdateDate)) costUpdateDate,
				IFNULL(MAX(nh.influence),0) AS trueReach,
				IFNULL(MAX(nh.follower),0) AS follower,
				IFNULL(MAX(nh.adPrice),0) AS adPrice
			FROM
				numbersHistory nh
			WHERE
				nh.userID='" . $data->id . "'
				AND nh.costUpdateDate IS NOT NULL
			GROUP BY DATE(FROM_UNIXTIME(nh.costUpdateDate))
			ORDER BY DATE(FROM_UNIXTIME(nh.costUpdateDate)) DESC
			LIMIT 0,15
		";
		DB::sql($sql);

		$kh = $this->get("DB")->result;

		$history = array("dates"=>array(),"reachs"=>array(),"followers"=>array(),"costs"=>array());

		if (count($kh)){
			for($i=count($kh)-1;$i>-1;$i--){
				$history["dates"][] = date("d.m.Y",strtotime($kh[$i]["costUpdateDate"]));
				$history["reachs"][] = $kh[$i]["trueReach"];
				$history["followers"][] = $kh[$i]["follower"];
				$history["costs"][] = $kh[$i]["adPrice"];
			}
		}

		$this->set("dates",$history["dates"]);
		$this->set("reachs",$history["reachs"]);
		$this->set("followers",$history["followers"]);
		$this->set("costs",$history["costs"]);
	}

	function admPpdateGroup()
	{
		$this->hasPermission($this->get("PermissionTypes.addPublisher"));


		$sql = "
			UPDATE users 
                        SET groupId=:group 
                        WHERE  username=:id 
                         
		";


		DB::sql($sql, array(":id" => $this->get("POST.id"), ":group" => $this->get("POST.value")));

		$Groups = $this->get("DB")->result;

		die($Groups);
	}

	function SearchPublishers()
	{
		$this->hasPermission($this->get("PermissionTypes.publishers"));

		$this->set('pagetitle', 'Search Publishers');
		$this->set('content', 'searchpublishers');

		$Groups = CrmCommon::GetAllUserGroups();

		$TagGroups = CrmCommon::GetAllTags();
		$this->set("PublisherTags", $TagGroups);

		$this->set("Searched", '0');
		$this->set("Groups", $Groups);
		$this->set("userTypes", $this->getUserTypes());
	}

	function doSearchPublishers()
	{
		$this->hasPermission($this->get("PermissionTypes.publishers"));

		$Groups = CrmCommon::GetAllUserGroups();

		$TagGroups = CrmCommon::GetAllTags();
		$this->set("PublisherTags", $TagGroups);

		$this->set("Groups", $Groups);
		$this->set("userTypes", $this->getUserTypes());

		// Start All Search


		$this->set('pagetitle', 'Search Publishers');
		$this->set('content', 'searchpublishers');


		$PublisherTags = F3::get('POST.PublisherTags');
		$types         = F3::get('POST.typesArray');
		$groups        = F3::get('POST.groupsArray');
		$this->set("groups", $groups);
		$twitterUserName = F3::get('POST.twitterUserName');
		$this->set("twitterUserName", $twitterUserName);
		$UserName = F3::get('POST.UserName');
		$this->set("UserName", $UserName);
		$location = F3::get('POST.location');
		$this->set("location", $location);
		$EndDate = F3::get('POST.EndDate');
		$this->set("EndDate", $EndDate);
		$StartDate = F3::get('POST.StartDate');
		$this->set("StartDate", $StartDate);

		$publishers = CrmCommon::SearchPublishers(NULL, $twitterUserName, $UserName, $location, $StartDate, $EndDate, $PublisherTags, $groups);
		$this->set("Searched", '1');
		$this->set("publishers", $publishers);

	}

	function allPublishersGroup()
	{
		$this->hasPermission($this->get("PermissionTypes.publishers"));

		$this->set('pagetitle', 'Publishers');
		$this->set('content', 'allpublishers');

		$u = $this->get("SESSION.accID");


		$sql = "
			SELECT
				u.avatar,
				u.username,
				u.name,
				u.twitterid,
				u.type,
				n.follower,
				n.influence,
				n.adPrice AS cost,
				n.followerUpdateDate,
				n.costUpdateDate,
				ut.type AS typeName
			FROM users u
				LEFT JOIN numbers n ON n.userID = u.twitterid
				LEFT JOIN userType ut ON u.type = ut.ID
                                WHERE u.groupid IN (SELECT GroupID
                                                    FROM  groupMembers m
                                                    WHERE MemberID = :userID)";

		DB::sql($sql, array(":userID" => $u));

		$publishers = $this->get("DB")->result;

		$this->set("publishers", $publishers);
		$this->set("userTypes", $this->getUserTypes());
	}


	function addPublisher()
	{
		$this->hasPermission($this->get("PermissionTypes.addPublisher"));

		$this->set('pagetitle', 'Add Publisher');
		$this->set('content', 'addPublisher');

		$Groups = CrmCommon::GetAllUserGroups();

		$TagGroups = CrmCommon::GetAllTags();

		$this->set("PublisherTags", $TagGroups);
		$this->set("Groups", $Groups);
		$this->set("userTypes", $this->getUserTypes());
	}

	function addBulkPublisher()
	{
		$this->hasPermission($this->get("PermissionTypes.addPublisher"));

		$this->set('pagetitle', 'Add Publisher');
		$this->set('content', 'addBulkPublisher');

		$Groups    = CrmCommon::GetAllUserGroups();
		$TagGroups = CrmCommon::GetAllTags();

		$this->set("PublisherTags", $TagGroups);
		$this->set("Groups", $Groups);
		$this->set("userTypes", $this->getUserTypes());
	}

	function savePublisher()
	{

		$this->hasPermission($this->get("PermissionTypes.addPublisher"));
		try {

			//sleep(15);

			$username      = F3::get('POST.username');
			$update        = F3::get('POST.update');
			$publisherTags = F3::get('POST.publisherTags');
			$groupId       = F3::get('POST.groupId');
			$forcedCalc    = F3::get('POST.forcedCalc');
			$publisherType = 1;

			CrmCommon::UserLog("Save Publisher " . $username, LogActions::AddUser);

			$user      = new Axon("users");
			$userStats = new Axon("numbers");

			//$getDataResults1 = CrmCommon::GetTwitterData($username);
			$data = CrmCommon::GetFromCalcAPI("calculate",$username,$forcedCalc);

			if (!isset($data->responseCode) || $data->responseCode != 200){
				die(json_encode(array("error" => 1, "description" => "Calculation API Error ", "username" => "_500", "data" => $data)));
			}

			$data = $data->responseBody;

			if ($data->status != "restarted" && $data->status != "calculation-start" && $data->status != "reseted" && $data->status != "calculated"){
				die(json_encode(array("error" => $data->status,"detail" => $data->detail)));
			}

			$cost           = null;
			$true_reach     = 0;
			$follower       = 0;
			$costUpdateDate = null;
			$costUpdateDateDifference = null;

			if ($data->status == "calculated"){
				$cost = $data->cost;
				$true_reach = $data->trueReach;
				$costUpdateDate = $data->calculateDate;

				$d1 = new DateTime(date("Y-m-d H:i:s",$costUpdateDate));
				$d2 = new DateTime(date("Y-m-d H:i:s",time()));
				$formatPattern = "";
				$difference = time() - $costUpdateDate;
				if ($difference < 60)
					$formatPattern = '%s seconds';
				elseif ($difference < 3600)
					$formatPattern = '%i minutes %s seconds';
				elseif ($difference < 86400)
					$formatPattern = '%h hours %i minutes %s seconds';
				else
					$formatPattern = '%d days %h hours %i minutes %s seconds';
				$difference = $d1->diff($d2);

				$costUpdateDateDifference =  $difference->format($formatPattern);
			}
			//die(print_r($data,true));
			if (is_object($data->tdata) && isset($data->tdata->data)){
				$data->tdata = $data->tdata->data;
				//die(print_r($data,true));
			}

			// Check if user Exist
			if ($data->status != "restarted" && (!is_object($data->tdata) || !isset($data->tdata->id))) {
				die(json_encode(array("error" => 1, "description" => "User Name Not Found ", "username" => $username, "api-data" => $data->tdata)));
			}

			$username = $data->tdata->screen_name;

			// Load User
			$user->load(array('username=:uid', array(':uid' => $username)));

			$userStats->load(array('userID=:uid', array(':uid' => $data->tdata->id)));
			//$farmDB         = $this->get("farmdbname");

			if (isset($data->tdata->followers_count) && is_numeric($data->tdata->followers_count)) $follower = $data->tdata->followers_count;
			/*
			DB::sql("SELECT * FROM {$farmDB}.user WHERE username=:username", array(":username" => $username));
			$u = $this->get("DB")->result;
			if (!count($u)) {
				DB::sql("INSERT INTO {$farmDB}.user(username,confirmed) VALUES(:username,1)", array(":username" => $username));
			}
			DB::sql("SELECT * FROM {$farmDB}.twitterInfo WHERE screenname=:username", array(":username" => $username));
			$twitterInfo = $this->get("DB")->result;
			if (!count($twitterInfo)) {
				DB::sql("INSERT INTO {$farmDB}.twitterInfo(screenname,twid,follower,following) VALUES(:screenname,{$getDataResults1->id},{$getDataResults1->followers_count},{$getDataResults1->friends_count})", array(":screenname" => $username));
				DB::sql("UPDATE {$farmDB}.twitterInfo ti, {$farmDB}.user u SET ti.user = u.id WHERE ti.screenname = u.username");
			} else {
				$sql = "
				SELECT kh.jdate,kh.trueReach,kh.cost,ti.id,tu.cstate
				FROM {$farmDB}.kloutHistory kh
					LEFT JOIN {$farmDB}.twitterInfoKlout tik ON tik.id = kh.twitterInfoKlout
					LEFT JOIN {$farmDB}.twitterInfo ti ON ti.id = tik.twitterInfo
					LEFT JOIN {$farmDB}.twitterUpdate tu ON tu.twitterInfo = ti.id
				WHERE
					ti.screenname = :username
				ORDER BY kh.jdate DESC
				LIMIT 0,1
			";
				DB::sql($sql, array(":username" => $username));
				$r = $this->get("DB")->result;
				//die(print_r($sql));

				if (count($r) && $r[0]["cstate"] == "f") {
					//$cost = number_format($r[0]["cost"],2);
					//$true_reach = $r[0]["trueReach"];
					//$costUpdateDate = $r[0]["jdate"];
					$sql = "UPDATE {$farmDB}.twitterInfo SET follower = {$getDataResults1->followers_count}, following = {$getDataResults1->friends_count}, twid = {$getDataResults1->id}, screenname = '{$username}' WHERE id = " . $r[0]["id"];
					DB::sql($sql);
					$sql = "UPDATE {$farmDB}.twitterUpdate SET gstate = null, lastGetDate = null, cstate = null, lastCalcDate = null WHERE twitterInfo = " . $r[0]["id"];
					//die($sql);
					DB::sql($sql);
				}

			}
			*/

			$user->twitterid = $data->tdata->id;
			$user->username  = $data->tdata->screen_name;
			$user->name      = $data->tdata->name;
			$user->avatar    = $data->tdata->profile_image_url;
			$user->bio       = $data->tdata->description;
			$user->location  = $data->tdata->location;
			$user->type      = $publisherType;

			if (!is_null($groupId)) {
				$user->groupId = $groupId;
			}
			$user->save();

			if (!$update) {
				$tempID = $user->_id;
				if ($tempID == NULL) {
					$tempID = $user->ID;
				}

				$UsersTags = new Axon("UsersTags");
				$UsersTags->erase('UserID=' . $tempID);

				if (is_array($publisherTags)){
					foreach ($publisherTags as $key => $value) {
						$UsersTags->UserID = $tempID;
						$UsersTags->TagID  = $value;
						$UsersTags->save();
					}
				}
			}


			$userStats->userID    = $data->tdata->id;
			$userStats->follower  = $follower;
			$userStats->following = $data->tdata->friends_count;
			$userStats->influence = $true_reach;
			$userStats->adPrice            = $cost;
			$userStats->followerUpdateDate = time();
			$userStats->costUpdateDate     = $costUpdateDate;
			$userStats->save();
			if (!is_null($cost)){
				$this->createNumbersHistory($userStats);
			}

			CrmCommon::UserLog("Saved Publisher " . $username, LogActions::AddedUser);

			if ($user->dry() || $user->ID == null) {
				die(json_encode(array("error" => 0, "isnew" => true, "username" => $username, "followers_count" => $follower)));
			} else {
				die(json_encode(array(
					"error"              => 0,
					"isnew"              => false,
					"followers_count"    => $follower,
					"true_reach"         => (is_null($cost) ? "Calculating" : $true_reach),
					"cost"               => (is_null($cost) ? "Calculating" : $cost),
					"followerUpdateDate" => date("d.m.Y H:i", $userStats->followerUpdateDate),
					"costUpdateDate"     => (is_null($costUpdateDate) ? "Calculating" : date("d.m.Y H:i", $costUpdateDate)),
					"costUpdateDateDifference" => $costUpdateDateDifference,
					"username"           => $username
				)));
			}


		} catch (Exception $exc) {
			die(json_encode(array("error" => 1, "description" => "Error ", "username" => "_500")));
		}
	}

	function deletePublisher()
	{
		$this->hasPermission($this->get("PermissionTypes.deletePublisher"));

		$twitterid = F3::get('POST.twitterid');
		$user      = new Axon("users");
		$user->load(array('twitterid=:uid', array(':uid' => $twitterid)));
		$user->erase();
		$userStats = new Axon("numbers");
		$userStats->load(array('userID=:uid', array(':uid' => $twitterid)));
		$userStats->erase();
		die(json_encode(array("error" => 0)));
	}

	private $_request;
	private $_requestVars;

	public function addPublisherScore(){
		require_once("rest.php");

		$this->_request = RestUtils::processRequest();
		$this->_requestVars = (object)$this->_request->getRequestVars();

		$data = json_decode($this->_requestVars->data);
		if (strlen($data->screen_name) && is_object($data->score_data)){
			$user      = new Axon("users");
			$userStats = new Axon("numbers");

			$user->load(array('username=:uid', array(':uid' => $data->screen_name)));

			if (!$user->dry()){
				$userStats->load(array('userID=:uid', array(':uid' => $user->twitterid)));

				if (!$userStats->dry()){
					$userStats->userID    = $user->twitterid;
					$userStats->influence = $data->score_data->trueReach;
					$userStats->adPrice            = round($data->score_data->cost,2);
					$userStats->costUpdateDate     = time();
					$userStats->save();
					$this->createNumbersHistory($userStats);
				}
			}
			unset($user,$userStats);
		}
		echo "ok";
		unset($data);
		exit;
	}

	private function createNumbersHistory($numbers){
		$nh = new Axon("numbersHistory");
		$fields = array("userID","follower","following","influence","adPrice","followerUpdateDate","costUpdateDate");
		foreach($fields as $field){
			$nh->$field = $numbers->$field;
		}
		$nh->save();
	}
}

?>
