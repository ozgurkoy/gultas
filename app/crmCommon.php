<?php

class CrmCommon extends Crm{
    
    /**
     * Get all basic user data from twitter
     * @param type $username
     * @return Twitter Object
     */
     static function GetTwitterData($username){
		
                try {
		CrmCommon::UserLog("Get Twitter Data ".$username, LogActions::GetTwitter);
                require_once str_replace("/app","", __DIR__).'/app/twitteroauth/twitteroauth.php';
                
		DB::sql("SELECT * from twitterKeys where active=1");
		$twitterKeys = DB::sql("SELECT * from twitterKeys where active=1");
		$maxKeys = count($twitterKeys);
                $twittrError = FALSE;
                $lastKeyIndex = 0;
                do{
                
		$appSecret = $twitterKeys[$lastKeyIndex]["app_secret"];
		$appSecretKey = $twitterKeys[$lastKeyIndex]["app_secret_key"];
		$consumerKey = $twitterKeys[$lastKeyIndex]["consumer_key"];
		$consumerKeySecret = $twitterKeys[$lastKeyIndex]["consumer_key_secret"];

		
                $connection = new TwitterOAuth($appSecret, $appSecretKey, $consumerKey, $consumerKeySecret);
                $getDataResults1 = $connection->get('https://api.twitter.com/1.1/users/show.json?screen_name='.$username);
                CrmCommon::UserLog("User Response ".$username." - ".print_r($getDataResults1,TRUE), LogActions::GetTwitter);
                //        print_r($getDataResults1->errors[0]);
                  //      exit();
                
                // Keys Error
                    if(isset($getDataResults1->errors))
                    {
                        // Invalid or expired token [code] => 89
                        if($getDataResults1->errors[0]->code == 89)
                        {
                            $twittrError = TRUE;
                            $args = array(':app_secret'=> $appSecret,':app_secret_key'=>$appSecretKey,':consumerKey'=> $consumerKey,':consumer_key_secret'=> $consumerKeySecret,':error'=> $getDataResults1->errors[0]->message);
                            $sql_update = "UPDATE `twitterKeys` SET `active`=0,Error=:error WHERE `app_secret`=:app_secret AND `app_secret_key`=:app_secret_key AND `consumer_key`=:consumerKey AND `consumer_key_secret`=:consumer_key_secret";
                            //DB::sql($sql_update,$args);

                            $updated = DB::sql($sql_update,$args); 
                            if(!$updated)
                            {
                                die(json_encode(array("error"=>1,"description"=>"Twitter Error, unable to save to DB ","username"=>$username)));
                            }
                        }
                        
                        $errorFullMessage = "Twitter Error ".$getDataResults1->errors[0]->code. " :".$getDataResults1->errors[0]->message;
                        die(json_encode(array("error"=>1,"description"=>$errorFullMessage,"username"=>$username)));
                        /*       
                                (
                           [code] => 63
                           [message] => User has been suspended
                       )
                         * 
                         */
                        
                        print_r($getDataResults1);
                        exit();
                    }
                    else
                    {
                        $twittrError = FALSE;
                    }
                    $lastKeyIndex = $lastKeyIndex +1;
                    
                    if($maxKeys == $lastKeyIndex)
                    {
                        die(json_encode(array("error"=>1,"description"=>"Invalid or expired Twitter token ","username"=>$username)));
                    }
                }
                while($twittrError == TRUE);
                
                return $getDataResults1;
                
                }
                catch (Exception $exc)
                {
                    
                }
     }

     /**
      * Get All Tages for the current user
      * @return type
      */
     static function GetAllTags()
     {
         $sql = "
			SELECT L.ID,IFNULL(g.GroupName,'Basic') GroupName, L.GroupID,L.typeName
                        FROM TagList L 
                        left Join groups g ON g.ID = L.GroupID 
                        where L.GroupID = 0 OR g.ID in (SELECT m.GroupID
                        FROM groupMembers m
                        WHERE m.MemberID =    
		". $_SESSION["accID"];
                $sql = $sql.")";

		//DB::sql($sql);

                $TagGroups = DB::sql($sql);
                
                
                return $TagGroups;

     }
     
     static function GetAllPublisherTags($publisherID)
     {
         $sql = "
			SELECT group_concat(TagID separator ', ') tagsArray
                        FROM UsersTags  
                        WHERE UserID =    
		". $publisherID;
                $sql = $sql." Group by UserID";

		//DB::sql($sql);

                $TagGroups = DB::sql($sql);
                
                if($TagGroups)
                {
                    return $TagGroups[0]['tagsArray'];
                }else
                {
                    return "";
                }
     }
     
     static function GetAllUserGroups()
     {
           $sql = "
			SELECT g.ID, g.GroupName
                        FROM groups g, groupMembers m
                        WHERE m.GroupID = g.ID
                        AND m.MemberID = 
                         
		". $_SESSION["accID"];
                
                

		$Groups = DB::sql($sql);
		
                return $Groups;
     }
     
     static function GetAllUserLists($WithMembersCount = FALSE)
     {
         if($WithMembersCount)
         {
                $sql = "
			SELECT lst.* , count(lst_usr.UserID)
                        FROM PublisherLists lst
                        left Join List_To_Users lst_usr ON lst.ID = lst_usr.ListID 
                        WHERE lst.Owner =
		". $_SESSION["accID"];
                
                $sql = $sql." group by lst.ID";
                
		$Groups = DB::sql($sql);
		
                return $Groups;
             
         }else{
             $PublisherLists = new Axon("PublisherLists");
             return $PublisherLists->afind("Owner=".$_SESSION["accID"]);
         }
         
     }
     
     static function SearchPublishers($twitterid =NULL,$twitterUserName = NULL,$UserName=NULL,$location=NULL,$StartDate=NULL,$EndDate=NULL,$Tags=NULL,$groups=NULL)
     {  
                if(strlen($groups) == 0)
                  $groups = "
			SELECT m.GroupID
                        FROM groupMembers m
                        WHERE  m.MemberID =  
                        ". $_SESSION["accID"];
                
                $sql = "
			SELECT
                                u.ID,
				u.avatar,
				u.username,
				u.name,
				u.twitterid,
				u.type,
				n.follower,
				n.influence,
				n.adPrice AS cost,
				n.followerUpdateDate,
				n.costUpdateDate,
                                u.groupId,
                                IFNULL(group_concat(tagList.typeName separator ', '),'No Tags') Tags
			FROM users u 
				LEFT JOIN numbers n ON n.userID = u.twitterid
                                LEFT JOIN UsersTags utag ON utag.UserID = u.ID
                                LEFT JOIN TagList tagList ON tagList.ID = utag.TagID 
                                
                        WHERE  1=1
		";
                if($twitterid)
                        $sql .= "AND u.twitterid =". $twitterid ." ";
                if(strlen($groups) > 0)
                        $sql .= "AND u.groupId IN (". $groups .")";
                if(count($Tags) > 0)
                        $sql .= "AND tagList.ID IN (". implode(',',$Tags) .")";
                if(strlen($UserName) > 0)
                        $sql .= "AND u.name LIKE '%". $UserName ."%'";
                if(strlen($twitterUserName) > 0)
                        $sql .= "AND u.username LIKE '%". $twitterUserName ."%'";
                if(strlen($location) > 0)
                        $sql .= "AND u.location LIKE '%". $location ."%'";
                if(strlen($StartDate) > 0)
                        $sql .= "AND n.followerUpdateDate >= UNIX_TIMESTAMP('". $StartDate ."') ";
                if(strlen($EndDate) > 0)
                        $sql .= "AND n.followerUpdateDate <= UNIX_TIMESTAMP('". $EndDate ."') ";
                
                $sql.= "Group by u.ID";
                // minimum one row cause using group_concat
                $resaults = DB::sql($sql);
                //CrmCommon::UserLog($sql, LogActions::Search);
                
                
                if(isset($resaults[0]['ID']))
                    return $resaults;

                
                return NULL;
     }
     
     
     static function UserLog($msg,$LogActions)
     {
         $log = new Axon("MembersLog");
         $log->Actiontype = $LogActions;
         $log->Message = $msg;
         $log->User = $_SESSION["accID"];
         $log->save();
         
     }
     
     static function GetUserLog($user=NULL,$LogActions=NULL)
     {
         $log = new Axon("MembersLog");
         
         return $log->afind();
         
     }

	public static function GetFromCalcAPI($operation,$username,$forcedCalc=null){
		global $main;

		require_once('RestRequest.php');

		$params = array(
			"screen_name"=>$username,
			"consumer_key"=>"",
			"consumer_secret"=>"",
			"app_secret"=>"",
			"app_secret_key"=>"",
			"environment_code"=>$main->get("calcAPIEnvironmentCode"),
			"push_address"=>$main->get("calcAPIPushAddress"),
			"minCost"=>0,
			"maxCost"=>0
		);

		if (strlen($forcedCalc)) $params["timeOut"] = 1;

		$request = new RestRequest($main->get("calcAPIServer") . $operation, 'POST', array(
			"data"=>  json_encode($params)
		),"json");
		$request->execute();

		$r = json_decode($request->responseBody);
		if (is_object($r))
			return $r;
		else
			return $request->responseBody;
	}
     
     
}

class LogActions
{
    const Login = 0;
    const logOut = 1;
    const Search = 2;
    const AddUser = 3;
    const AddedUser = 10;
    const UpdateUser = 4;
    const DeleteUser = 5;
    const AddTag = 6;
    const EditTag = 7;
    const DeleteTag = 8;
    const GetTwitter = 9;
    
    
}


?>