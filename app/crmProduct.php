<?php
class CrmProduct extends Crm {
	function allProducts(){
		$this->set('pagetitle','Tüm Ürünler');
		$this->set('content','allproducts');

		DB::sql("SELECT * FROM products");

		$this->set("products",$this->get("DB")->result);
	}

	function addProduct(){
		$this->set('pagetitle','Ürün Ekleme / Düzenleme');
		$this->set('content','addproduct');

		$pid = $this->get("PARAMS.pid");
		if (strlen($pid) == 0){
			$pid = 0;
			$this->set("PARAMS.pid",0);
		}

		$product = new Axon("products");
		$product->load(array('ID=:pid',array(':pid'=>$pid)));

		$this->set("product",$product);
	}

	function saveProduct(){
		F3::set('POST',F3::scrub($_POST));

		$product = new Axon("products");
		$product->load(
			array(
				'ID=:pid',
				array(
					':pid'=>intval($this->get('POST.ID'))
				)
			)
		);
		$product->copyFrom('POST');
		$product->save();

		if (intval($this->get('POST.ID')) == 0){
			$product=new Axon('products');
			$product->def('newProductID','MAX(ID)');
			$product->load();
			die(json_encode(array("error"=>0,"newProductID"=>$product->newContactID)));
		} else {
			die(json_encode(array("error"=>0)));
		}
	}

	function deleteProduct(){
		$pid = F3::get('POST.cid');
		$product = new Axon("products");
		$product->load(array('ID=:pid',array(':pid'=>$pid)));
		$product->erase();
		die(json_encode(array("error"=>0)));

	}
}
?>
