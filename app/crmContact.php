<?php
class CrmContact extends Crm {
	function allContacts(){
		$this->set('pagetitle','Tüm Kontak Bilgileri');
		$this->set('content','allcontacts');

		DB::sql("
			SELECT
				c.*,
				cu.name AS accountName
			FROM contacts c
				LEFT JOIN customers cu ON cu.ID = c.account
		");

		$this->set("contacts",$this->get("DB")->result);

	}

	function addContacts(){
		$this->set('pagetitle','Kontak Ekleme / Düzenleme');
		$this->set('content','addcontact');

		$cid = $this->get("PARAMS.cid");
		if (strlen($cid) == 0){
			$cid = 0;
			$this->set("PARAMS.cid",0);
		}

		$contact = new Axon("contacts");
		$contact->load(array('ID=:cid',array(':cid'=>$cid)));
		$contact->dateOfBirth = date("d/m/Y", strtotime($contact->dateOfBirth));

		DB::sql("SELECT * FROM customers ORDER BY `name` ASC");
		$customers = $this->get("DB")->result;

		$this->set("contact",$contact);
		$this->set("customers2",$customers);
		$this->set("cities",$this->getCities());
		$this->set("countries",$this->getCountries());
	}

	function saveContact(){
		require_once 'upload.php';
		$upload_handler = new UploadHandler(array("upload_dir"=>  str_replace("/app","/",dirname(__FILE__)) . '/photo/','upload_url' => dirname($_SERVER['PHP_SELF']) . '/photo/'));

		$info = $upload_handler->post();

		F3::set('POST',F3::scrub($_POST));

		if (isset($info[0]->name) && strlen($info[0]->name)>0){
			$this->set("POST.file",$info[0]->name);
		}



		$contact = new Axon("contacts");
		$contact->load(
			array(
				'ID=:cid',
				array(
					':cid'=>intval($this->get('POST.ID'))
				)
			)
		);
		$contact->copyFrom('POST');
		$contact->save();

		if (intval($this->get('POST.ID')) == 0){
			$contact=new Axon('contacts');
			$contact->def('newContactID','MAX(ID)');
			$contact->load();
			$this->reroute("/contactDetail/".$contact->newContactID);
		} else {
			$this->reroute("/contactDetail/".intval($this->get('POST.ID')));
		}
	}

	function contactDetail(){
		$this->set('pagetitle','Kontak Ekleme / Düzenleme');
		$this->set('content','contactdetail');

		$cid = $this->get("PARAMS.cid");

		DB::sql("
			SELECT
				c.*,
				cu.name AS customerName,
				cu.location AS customerAddress,
				ci.name AS cityName,
				co.country AS countryName
			FROM contacts c
				LEFT JOIN customers cu ON cu.ID = c.account
				LEFT JOIN cities ci ON ci.ID = c.city
				LEFT JOIN countries co ON co.id = c.country
			WHERE
				c.ID=$cid
			LIMIT 0,1
		");
		$contact = $this->get("DB")->result[0];
		$contact["dateOfBirth"] = date("d/m/Y", strtotime($contact["dateOfBirth"]));

		//die(print_r($contact));

		$this->set("contact",$contact);
	}

	function deleteContact(){
		$cid = F3::get('POST.cid');
		$contact = new Axon("contacts");
		$contact->load(array('id=:cid',array(':cid'=>$cid)));
		$contact->erase();
		die(json_encode(array("error"=>0)));

	}
}
?>
