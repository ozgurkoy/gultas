<?php
class CrmConsole extends Crm {
	function init(){
		$argv = $this->get("argv");
		if (count($argv) < 2){
			die("Hatalı kullanım! Gelen parametre sayısı: ".count($argv));
		} else {
			switch ($argv[1]){
				case "checkCashFlow":
					$this->checkCashFlow();
					break;
				default:
					die("unknown job type\n");
					break;
			}
		}
	}
	
	function checkCashFlow(){
		DB::sql("
			SELECT
				cf.id,
				CONCAT(
					CASE
						WHEN cf.ac = 'Y' THEN co.name
						ELSE cu.name
					END,
					'<br>',
					cf.companyResponsible,
					'<br>',
					cf.responsiblePhone
				) AS company,
				CASE
					WHEN cf.brandID > 0 THEN cb.title
					ELSE cf.otherBrand
				END AS brand,
				CASE
					WHEN cf.campaignID > 0 THEN ca.title
					ELSE cf.otherCampaign
				END AS campaign,
				cf.campDate,
				cf.campCost,
				cf.dueDate,
				CONCAT(m.name,' ',m.surname) responsible,
				cf.notes,
				CONCAT(DATEDIFF(cf.dueDate,CURDATE()), ' gün') dateLeft
			FROM
				cashFlow cf
				LEFT JOIN " . $this->get("admdbname") . ".companyBrand cb ON cf.ac = 'Y' AND cb.id = cf.brandID
				LEFT JOIN " . $this->get("admdbname") . ".company co ON cf.ac = 'Y' AND co.id = cf.companyID
				LEFT JOIN " . $this->get("admdbname") . ".campaign ca ON cf.ac = 'Y' AND ca.id = cf.campaignID
				LEFT JOIN customers cu ON cf.ac <> 'Y' AND cu.ID = cf.companyID
				LEFT JOIN members m ON m.id = cf.memberID
			WHERE cf.dueDate BETWEEN CURDATE() AND DATE_ADD(CURDATE(),INTERVAL 2 DAY)
		");

		$this->set("cashFlows",$this->get("DB")->result);
		
		//,jorder,jdate
		DB::sql("INSERT INTO " . $this->get("admdbname") . ".mail(status,jorder,jdate,`to`,subject,message,reqDate,state,checkcount) VALUES(:status,:jorder,:jdate,:to,:subject,:message,:reqDate,:state,:checkcount)",array(
			":status"=>10,
			":jorder"=>1,
			":jdate"=>time(),
			":to"=>"murat@admingle.com",
			":subject"=>"Günlük Ödeme Raporu - " . date("d.m.Y",time()),
			":message"=>Template::serve('mail/mail.html'),
			":reqDate"=>time(),
			":state"=>'w',
			":checkcount"=>0
		));
		die("bitti");
	}
}
?>
