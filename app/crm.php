<?php

/**
	Not ideal, but we're lumping all the route handlers in one class so it's
	easier to understand the application logic and how the methods interact
	with the index.php main controller.
**/
class Crm extends F3instance {

	public static $permissions = array();

	function show() {
		$this->set('pagetitle','Home Page');
		$this->set('content','dashboard');

		if ($this->checkPermission($this->get("PermissionTypes.dashboard"))){
			DB::sql("
				SELECT
					st.type,
					count(*) cnt
				FROM status s
					INNER JOIN statusType st on st.ID = s.status
				GROUP BY
					st.type
				ORDER BY
					st.type ASC
			");
			$this->set("statusCounts",$this->get("DB")->result);

			DB::sql("
				SELECT `date`
				FROM status s
				WHERE s.status = 6
				GROUP BY `date`
				ORDER BY `date`
			");
			$this->set("dates",$this->get("DB")->result);

			DB::sql("
				SELECT
					CONCAT_WS(' ',m.name, m.surname) manager_name,
					d.`date`,
					(SELECT count(*) FROM status ss WHERE ss.manager = m.ID AND ss.date = d.date AND ss.status = s.status) cnt
				FROM (
						SELECT `date`
						FROM status s
						WHERE s.status = 6 AND `date` > '" . date('Y-m-d', strtotime("-1 month")) . "'
						GROUP BY `date`
					) d
					CROSS JOIN status s
					LEFT JOIN members m ON m.ID = s.manager
				WHERE
					s.status = 6
					AND s.`date` > '" . date('Y-m-d', strtotime("-1 month")) . "'
				GROUP BY
					CONCAT_WS(' ',m.name, m.surname),
					d.`date`
				ORDER BY
					CONCAT_WS(' ',m.name, m.surname),
					s.date
			");
			$rows = $this->get("DB")->result;
			$members = array();
			foreach ($rows as $row){
				if (!isset($members[$row['manager_name']])){
					$members[$row['manager_name']] = array();
				}
				$members[$row['manager_name']][] = $row["cnt"];
			}
			$this->set("memberStats",$members);
		} else {
			$this->reroute("/SearchPublishers");
		}
	}

	function login() {
		// echo hash('sha256', '7116182f690da88da88fccde18e6817b8b2efa95d9472f1c48023d8a816738ce4ba');
		$this->set('pagetitle','Login');
	}

	function doLogin() {
		$username = F3::get('POST.username');
		$password = F3::get('POST.password');

		$user=new Axon('members');
		$user->load(array('username=:uid',array(':uid'=>$username)));
		if ($user->dry()){
			$this->reroute("/?invaliduser");
		}else{
            $hash = hash('sha256', $password);
			$salt = $user->salt;

			$hash = hash('sha256', $salt . $hash);
                    /*    
                        print_r($hash);
                        print_r('<br/>');
                        print_r($user->password);
                        die();
                        */
                        
			if($hash != $user->password) //incorrect password
			{
				$this->reroute("/?passwordnotmatch");
			}
			else
			{
				session_regenerate_id(); //this is a security measure
				$this->set("SESSION.valid","Y");
				$this->set("SESSION.userid",$username);
				$this->set("SESSION.accID",$user->id);

				if (F3::get('POST.remember') == "1"){
					setcookie("admCRM_LU", $username, time()+50400);
					setcookie("admCRM_LK", $hash, time()+50400);
				}
                                
				$this->reroute("/?success");
			}
		}
	}

	function logOut(){
		$this->clear("SESSION");
		$this->set("COOKIE.admCRM_LU",null);
		$this->set("COOKIE.admCRM_LK",null);
		foreach ($_COOKIE as $name => $value) {
			setcookie($name, '', 1);
		}
		$this->reroute("/");
	}

	function getSectors(){
		//$sectors=new Axon('sector');
		//return $sectors->afind();
		DB::sql("SELECT id, name FROM sector ORDER BY name ASC");
		return $this->get("DB")->result;
	}

	function getCustomerTypes(){
		DB::sql("SELECT ID, name FROM customertype ORDER BY name ASC");
		return $this->get("DB")->result;
	}

	function getUserTypes(){
		DB::sql("SELECT * FROM userType ORDER BY `type` ASC");
		return $this->get("DB")->result;
	}

	function getStatusTypes(){
		DB::sql("SELECT * FROM statusType ORDER BY `type` ASC");
		return $this->get("DB")->result;
	}

	function getCities(){
		DB::sql("SELECT * FROM cities ORDER BY `name` ASC");
		return $this->get("DB")->result;
	}

	function getCountries(){
		DB::sql("SELECT * FROM countries ORDER BY `country` ASC");
		return $this->get("DB")->result;
	}

	function getMembers(){
		DB::sql("SELECT id, username, name, surname, email, gsm FROM members ORDER BY `name` ASC, `surname` ASC");
		return $this->get("DB")->result;
	}
	
	function error404(){
	    die(Template::serve('error-404.html'));
	}

	function beforeroute() {
		$this->set('DB',new DB('mysql:host={{ @dbhost }};port={{ @dbport }};dbname={{ @dbname }}',$this->get("dbuser"),$this->get("dbpass")));

		if ($this->get("COOKIE.admCRM_LU") && $this->get("COOKIE.admCRM_LK") && !$this->get('SESSION.accID')){

			$username = $this->get("COOKIE.admCRM_LU");
			$key = $this->get("COOKIE.admCRM_LK");

			$user=new Axon('members');
			$user->load(array('username=:uid AND password=:key',array(':uid'=>$username,':key'=>$key)));

			if (!$user->dry()){
				$this->set("SESSION.valid","Y");
				$this->set("SESSION.userid",$username);
				$this->set("SESSION.accID",$user->id);
			}

		}
                
		if ($this->get('SESSION.accID')){
			DB::sql(
				"SELECT m.name FROM permissions p INNER JOIN modules m ON m.ID = p.module WHERE p.member = :uID",
				array(":uID"=>$this->get("SESSION.accID"))
			);
			foreach (F3::get('DB->result') as $row) {
				self::$permissions[] = $row["name"];
			}

			$this->set('sectors',$this->getSectors());
			$this->set('customertypes',$this->getCustomerTypes());
			$this->set('usertypes',$this->getUserTypes());
		}
	}

	function afterroute() {
		// Serve master template; layout.htm is located in the directory
		// pointed to by the GUI global variable
                
		if (count(self::$permissions)){
			echo Template::serve('layout.htm');
		} else {
			echo Template::serve('login.htm');
		}
	}

	public static function checkPermission($p){
		if (in_array($p, self::$permissions))
			return true;
		else
			return false;

	}

	public static function hasPermission($p){
		if (!self::checkPermission($p)){
			die(Template::serve('nopermission.htm'));
		}
	}

	function markafoni(){
		$this->set('pagetitle','Markafoni');
		$this->set('content','markafoni');

		DB::sql("SELECT * from hashtags ORDER BY tweetid");
		$this->set("tweets",$this->get("DB")->result);
	}

	function markafoniUpdate(){
		require_once str_replace("/app","", __DIR__).'/markafoni/twitteroauth/twitteroauth.php';

		DB::sql("SELECT * from twitterKeys");
		$twitterKeys = $this->get("DB")->result;
		$lastKeyIndex = 0;
		$appSecret = $twitterKeys[$lastKeyIndex]["app_secret"];
		$appSecretKey = $twitterKeys[$lastKeyIndex]["app_secret_key"];
		$consumerKey = $twitterKeys[$lastKeyIndex]["consumer_key"];
		$consumerKeySecret = $twitterKeys[$lastKeyIndex]["consumer_key_secret"];

		DB::sql("SELECT * from hashtags ORDER BY tweetid");
		$tweets = $this->get("DB")->result;
		echo "<pre>";
		foreach ($tweets as $tweet){
			//$dataResults1 = Web::http("GET http://api.twitter.com/1/statuses/show.json?id=". $tweet["tweetid"]);
			//$getDataResults1 = json_decode($dataResults1);
			$noerror = true;

			$connection = new TwitterOAuth($appSecret, $appSecretKey, $consumerKey, $consumerKeySecret);
			$getDataResults1 = $connection->get('statuses/show', array('id' => $tweet["tweetid"], 'include_entities' => '0'));

			print_r($getDataResults1);

			if (isset($getDataResults1->error) && strpos($getDataResults1->error,"Rate limit exceeded") !== false){
				echo "changing keys\n";
				$lastKeyIndex++;
				$appSecret = $twitterKeys[$lastKeyIndex]["app_secret"];
				$appSecretKey = $twitterKeys[$lastKeyIndex]["app_secret_key"];
				$consumerKey = $twitterKeys[$lastKeyIndex]["consumer_key"];
				$consumerKeySecret = $twitterKeys[$lastKeyIndex]["consumer_key_secret"];

				$connection = new TwitterOAuth($appSecret, $appSecretKey, $consumerKey, $consumerKeySecret);
				$getDataResults1 = $connection->get('statuses/show', array('id' => $tweet["tweetid"], 'include_entities' => '0'));

				print_r($getDataResults1);
			}

			if (isset($getDataResults1->error) && strpos($getDataResults1->error,"you are not authorized to see this status")){
				echo($getDataResults1->error . "\n");
				echo "Trying to check public API";
				$dataResults1 = Web::http("GET http://api.twitter.com/1/statuses/show.json?id=". $tweet["tweetid"]);
				$getDataResults1 = json_decode($dataResults1);
				print_r($getDataResults1);
			}

			if (isset($getDataResults1->error) || isset($getDataResults1->errors)){
				echo isset($getDataResults1->error) ? $getDataResults1->error : $getDataResults1->errors;
				$noerror = false;
			}

			if ($noerror){
				DB::sql("UPDATE hashtags SET rtcount=" .$getDataResults1->retweet_count.",lastupdated=" . time() ." WHERE tweetid=" . $tweet["tweetid"]);
				echo $tweet["tweetid"] . " - updated\n";
			}
/*
stdClass Object
(
    [request] => /1/statuses/show.json?id=233549656578203648
    [error] => Rate limit exceeded. Clients may not make more than 150 requests per hour.
)
 */
			unset($connection,$getDataResults1);
		}
		die();
	}
}
