<?php
class CrmCashFlow extends Crm {
	function allCashFlows(){
		$this->hasPermission($this->get("PermissionTypes.cashFlow"));
		
		$this->set('pagetitle','Nakit Akışı');
		$this->set('content','allcashflows');

		DB::sql("
			SELECT
				cf.id,
				CASE
					WHEN cf.ac = 'Y' THEN co.name
					ELSE cu.name
				END AS company,
				CASE
					WHEN cf.brandID > 0 THEN cb.title
					ELSE cf.otherBrand
				END AS brand,
				CASE
					WHEN cf.campaignID > 0 THEN ca.title
					ELSE cf.otherCampaign
				END AS campaign,
				cf.campDate,
				cf.campCost,
				cf.dueDate,
				CONCAT(m.name,' ',m.surname) responsible,
				cf.notes
			FROM
				cashFlow cf
				LEFT JOIN " . $this->get("admdbname") . ".companyBrand cb ON cf.ac = 'Y' AND cb.id = cf.brandID
				LEFT JOIN " . $this->get("admdbname") . ".company co ON cf.ac = 'Y' AND co.id = cf.companyID
				LEFT JOIN " . $this->get("admdbname") . ".campaign ca ON cf.ac = 'Y' AND ca.id = cf.campaignID
				LEFT JOIN customers cu ON cf.ac <> 'Y' AND cu.ID = cf.companyID
				LEFT JOIN members m ON m.id = cf.memberID
		");

		$this->set("cashFlows",$this->get("DB")->result);
		//die("<pre>".print_r($this->get("cashFlows"),true));
	}
	
	function addCashFlow(){
		$this->hasPermission($this->get("PermissionTypes.cashFlow"));
		
		$this->set('pagetitle','Nakit Akışı Ekle');
		$this->set('content','addcashflow');
		DB::sql("
			SELECT
				co.id,
				co.name AS title,
				contactName AS responsible,
				CONCAT(contactTel,' / ',contactGsm) AS phone,
				'Y' as ac
			FROM " . $this->get("admdbname") . ".company co
			ORDER BY co.name
		");
		$companies = array();
		foreach($this->get("DB")->result as $campaign){
			$companies[] = array(
				"id"=>$campaign["id"],
				"title"=>$campaign["title"],
				"responsible"=>$campaign["responsible"],
				"phone"=>$campaign["phone"],
				"ac"=>"Y");
		}
		DB::sql("
			SELECT
				id,
				name AS title,
				responsiblehead AS responsible,
				CONCAT(phone,' / ',gsm) AS phone,
				'N' AS ac
			FROM customers
			ORDER BY name
		");
		foreach($this->get("DB")->result as $campaign){
			$companies[] = array(
				"id"=>$campaign["id"],
				"title"=>$campaign["title"],
				"responsible"=>$campaign["responsible"],
				"phone"=>$campaign["phone"],
				"ac"=>"N");
		}
		
		$this->set("companies",$companies);
		$this->set("members",$this->getMembers());
		
		if ($this->exists("PARAMS.cid") && is_numeric($this->get("PARAMS.cid"))){
			$cashFlow = new Axon("cashFlow");
			$cashFlow->load(array('id=:cid',array(':cid'=>intval($this->get("PARAMS.cid")))));
			if (!$cashFlow->dry){
				$cashFlow->campDate = date("d/m/Y",strtotime($cashFlow->campDate));
				$cashFlow->dueDate = date("d/m/Y",strtotime($cashFlow->dueDate));
			}
			$this->set("cashFlow",$cashFlow);
			//die("<pre>".print_r($cashFlow,true));
		}
	}
	
	function getCompanyBrands(){
		$this->hasPermission($this->get("PermissionTypes.cashFlow"));
		
		$cid = $this->get("POST.cid");
		if (is_null($cid) || !is_numeric($cid)){
			die("invalid request");
		}
		
		DB::sql("
			SELECT
				cb.id,
				cb.title
			FROM " . $this->get("admdbname") . ".companyBrand cb
			WHERE (cb.active = 1 OR cb.active IS NULL) AND cb.company = $cid
			ORDER BY cb.title
		");
		
		$brands = array();
		$brands["brands"][] = array("id"=>"","title"=>"Seçin");
		foreach($this->get("DB")->result as $brand){
			$brands["brands"][] = array(
				"id"=>$brand["id"],
				"title"=>$brand["title"]);
		}
		$brands["brands"][] = array("id"=>-1,"title"=>"Diğer");
		die(json_encode($brands));
	}
	
	function getBrandCampaigns(){
		$this->hasPermission($this->get("PermissionTypes.cashFlow"));
		
		$bid = $this->get("POST.bid");
		if (is_null($bid) || !is_numeric($bid)){
			die("invalid request");
		}
		
		DB::sql("
			SELECT
				ca.id,
				ca.title,
				ca.startDate,
				((IFNULL(ca.budget,0) + IFNULL(ca.extraBudget,0) + IFNULL(ca.celebCost,0))*((100-IFNULL(ca.discountRate,0))/100)) AS cost
			FROM " . $this->get("admdbname") . ".campaign ca
			WHERE ca.companyBrand = $bid
			ORDER BY ca.title
		");
		$campaigns = array();
		$campaigns["campaigns"][] = array("id"=>"","title"=>"Seçin","date"=>"","cost"=>"");
		foreach($this->get("DB")->result as $campaign){
			$campaigns["campaigns"][] = array(
				"id"=>$campaign["id"],
				"title"=>$campaign["title"],
				"date"=>date("d/m/Y",strtotime($campaign["startDate"])),
				"cost"=>$campaign["cost"]);
		}
		
		$campaigns["campaigns"][] = array("id"=>-1,"title"=>"Diğer","date"=>"","cost"=>"");
		die(json_encode($campaigns));
	}
	
	function saveCashFlow(){
		$this->hasPermission($this->get("PermissionTypes.cashFlow"));
		
		$cid = $this->get("POST.id");
		$cashFlow = new Axon("cashFlow");
		if (strlen($cid)>0 && is_numeric($cid)){
			$cashFlow->load(array('id=:cid',array(':cid'=>$cid)));
		}
		$cashFlow->copyFrom('POST');
		$cashFlow->save();
		die(json_encode(array("error"=>0,"cid"=>$cashFlow->_id)));
	}
}
?>
