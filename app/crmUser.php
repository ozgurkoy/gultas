<?php
class CrmUser extends Crm {
	function editPermissions(){
		$this->hasPermission($this->get("PermissionTypes.admin"));

		$this->set('pagetitle','Kullanıcı Yetkilendirme');
		$this->set('content','permissions');

		$members = new Axon("members");
		$modules = new Axon("modules");
                $groups = new Axon("groups");
		
                $this->set("groups",$groups->afind());
                $this->set("members",$members->afind());
		$this->set("modules",$modules->afind());

	}

        // User Selected
	function getUserPermissions(){
		$this->hasPermission($this->get("PermissionTypes.admin"));

                
		$u = intval($this->get("POST.u"));
                
                $member = new Axon("members");
                $member->load("id=$u");

		$gravatar =  md5(strtolower(trim($member->email)));
		
		$permission = new Axon("permissions");
                $groups = new Axon("groupMembers");
                
                die(json_encode(array("error" => "0",
                                      "u" => $u,
                                      "result" => $permission->afind("member=$u"),
                                      "userData" => $member->afindone("id=$u"),
                                      "gravatar" => $gravatar,
                                      "userGroups" =>$groups->afind("MemberID=$u"))));
	}
        
        function getUserGroups(){
                
                $groups = new Axon("groupMembers");
                
                $u = $this->get("SESSION.accID");
                $userGroups = $groups->afind("MemberID=$u");
                
                die(json_encode($userGroups));
	}

        function updateUserGrops(){
		$this->hasPermission($this->get("PermissionTypes.admin"));

		$u = intval($this->get("POST.u"));
		$p = intval($this->get("POST.p"));
		$a = $this->get("POST.a");


		if (strtolower($a) == "checked")
			$s = "INSERT IGNORE INTO groupMembers(MemberID,GroupID) VALUES($u,$p);";
		else
			$s = "DELETE FROM groupMembers WHERE MemberID = $u AND GroupID = $p;";
		DB::sql($s);
		die(json_encode(array("error" => "0", "result" => "ok")));
	}
        
	function updateUserPermissions(){
		$this->hasPermission($this->get("PermissionTypes.admin"));

		$u = intval($this->get("POST.u"));
		$p = intval($this->get("POST.p"));
		$a = $this->get("POST.a");


		if (strtolower($a) == "checked")
			$s = "INSERT IGNORE INTO permissions(member,module) VALUES($u,$p);";
		else
			$s = "DELETE FROM permissions WHERE member = $u AND module = $p;";
		DB::sql($s);
		die(json_encode(array("error" => "0", "result" => "ok")));
	}

	function profile(){

		$this->set('pagetitle','Profil Güncelleme');
		$this->set('content','profile');

		$member = new Axon("members");
		$member->load("id=" . intval($this->get("SESSION.accID")));

		$this->set("member",$member);
		$this->set("gravatar","http://www.gravatar.com/avatar/" . md5(strtolower(trim($member->email))));
	}

	function saveProfile(){

		if (!is_null($this->get("POST.password"))){
			$password = $this->get("POST.password");
			$hash = hash('sha256', $password);
			$string = md5(uniqid(rand(), true));
			$salt = substr($string, 0, 3);
			$hash = hash('sha256', $salt . $hash);
			$this->set("POST.salt",$salt);
			$this->set("POST.password",$hash);
		}
		$member = new Axon("members");
		$member->load("id=" . intval($this->get("SESSION.accID")));
		$member->copyFrom('POST');
		$member->save();
		die(json_encode(array("error"=>0)));
	}
        
        function adminNewPassSet(){
            $this->hasPermission($this->get("PermissionTypes.admin"));

		
                $password = $this->get("POST.password");
                $hash = hash('sha256', $password);
                $string = md5(uniqid(rand(), true));
                $salt = substr($string, 0, 3);
                $hash = hash('sha256', $salt . $hash);
                
                
		$member = new Axon("members");
		$member->load("id=" . $this->get("POST.SelecteduserID"));
		$member->salt = $salt;
                $member->password = $hash;
		$member->save();
		die(json_encode(array("error"=>0)));
	}
        
        function adminUpdateProfile(){
            
		$this->hasPermission($this->get("PermissionTypes.admin"));

                /*
		if (!is_null($this->get("POST.password"))){
			$password = $this->get("POST.password");
			$hash = hash('sha256', $password);
			$string = md5(uniqid(rand(), true));
			$salt = substr($string, 0, 3);
			$hash = hash('sha256', $salt . $hash);
			$this->set("POST.salt",$salt);
			$this->set("POST.password",$hash);
		}*/
                
		$member = new Axon("members");
		$member->load("id=" . $this->get("POST.users"));
		$member->copyFrom('POST');
		$member->save();
		die(json_encode(array("error"=>0)));
	}
}
?>
