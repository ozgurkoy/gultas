<?php
class CrmCustomer extends Crm {
	function allNotes(){
		$this->hasPermission($this->get("PermissionTypes.firms"));

		$this->set('pagetitle','Firmalara Ait Tüm Notlar');
		$this->set('content','allnotes');

		DB::sql("
			SELECT
				n.ID,
				n.date,
				n.companyID,
				c.name as customerName,
				n.note,
				n.noted
			FROM notes n
				INNER JOIN customers c ON c.ID = n.companyID
			ORDER BY
				n.date DESC
		");
		$this->set("notes",$this->get("DB")->result);
	}

	function allCustomers(){
		$this->hasPermission($this->get("PermissionTypes.firms"));

		$this->set('pagetitle','Firmalar');
		$this->set('content','allcustomers');

		$sql = "
			SELECT
				c.*,
				ci.name as city
			FROM customers c
				LEFT JOIN cities ci ON c.city = ci.ID
		";
		switch($this->get("PARAMS.ctype")){
			case "s":
				$sql .= " WHERE c.sectorID=" . intval($this->get("PARAMS.tid"));
				break;
			case "t":
				$sql .= " WHERE c.type=" . intval($this->get("PARAMS.tid"));
				break;
			default:
				break;
		}
		DB::sql($sql);
		$this->set("customers",$this->get("DB")->result);
	}

	function allStatuses(){
		$this->hasPermission($this->get("PermissionTypes.firms"));

		$this->set('pagetitle','Firmalara Ait Tüm Durumlar');
		$this->set('content','allstatuses');

		DB::sql("
			SELECT
				s.ID,
				s.date,
				s.note,
				s.file,
				s.manager,
				s.campaign,
				st.type AS statusType,
				CONCAT_WS(' ',m.name,m.surname) AS manager_name,
				c.name AS company_name,
				c.ID AS company_id,
				m.username AS manager_username,
				IFNULL((SELECT CONCAT_WS(' ',campCode,title) AS title FROM " . $this->get("admdbname") . ".campaign camp WHERE camp.id=s.campaign),'-') AS campaignName
			FROM `status` s
				INNER JOIN statusType st ON st.ID = s.status
				INNER JOIN members m ON m.ID = s.manager
				INNER JOIN customers c ON c.ID = s.company
			ORDER BY date DESC
		");
		$this->set("statuses",$this->get("DB")->result);
	}

	function customerDetail(){
		$this->hasPermission($this->get("PermissionTypes.firms"));

		$this->set('pagetitle','Firma Detayı');
		$this->set('content','customerdetail');

		$cid = intval($this->get("PARAMS.cid"));


		DB::sql("
			SELECT
				c.*,
				s.name AS sectorName,
				ct.name AS typeName,
				status.date AS lastStatusDate,
				st.type AS lastStatusType,
				st.progress AS progressSize
			FROM
				customers c
				LEFT JOIN sector s ON s.ID = c.sectorID
				LEFT JOIN customertype ct ON ct.ID = c.type
				LEFT JOIN status ON status.company = c.ID
				LEFT JOIN statusType st ON st.ID = status.status
			WHERE
				c.ID=$cid
			ORDER BY status.date DESC
			LIMIT 0,1
		");
		$customer = $this->get("DB")->result;
		$customer = $customer[0];

		$progressDisplay = "";

		if ($customer["progressSize"] == '')
		{
			$progressDisplay = 'none';
		}

		//$this->set('adMingleDB',new DB('mysql:host={{ @admdbhost }};port={{ @admdbport }};dbname={{ @admdbname }}',$this->get("admdbuser"),$this->get("admdbpass")));
/*

		DB::sql("
			SELECT
				id,
				CONCAT_WS(' ',campCode,title) AS title
			FROM
				" . $this->get("admdbname") . ".campaign
			WHERE
				crmmember=" . $this->get("SESSION.accID") . "
			ORDER BY campCode
		");
		$campaigns = $this->get("DB")->result;
*/

		DB::sql("
			SELECT
				s.ID,
				s.date,
				s.note,
				s.file,
				s.manager,
				st.type AS statusType,
				CONCAT_WS(' ',m.name,m.surname) AS manager_name,
				c.name AS company_name,
				c.ID AS company_id,
				m.username AS manager_username
				FROM `status` s 
				INNER JOIN statusType st ON st.ID = s.status
				INNER JOIN members m ON m.ID = s.manager
				INNER JOIN customers c ON c.ID = s.company
			WHERE s.company='".$customer["ID"]."'
			ORDER BY s.date DESC
		");
		$statuses = $this->get("DB")->result;

		DB::sql("
			SELECT
				n.ID,
				n.date,
				n.companyID,
				n.note,
				n.noted
			FROM notes n
			WHERE companyID='".$customer["ID"]."'
			ORDER BY
				n.date DESC
		");
		$notes = $this->get("DB")->result;


		$this->set("progressDisplay",$progressDisplay);
		$this->set("customer",$customer);
		$this->set("statusTypes",$this->getStatusTypes());
		// $this->set("campaigns",$campaigns);
		$this->set("statuses",$statuses);
		$this->set("notes",$notes);
	}

	function addCustomer(){
		$this->hasPermission($this->get("PermissionTypes.editFirm"));

		$this->set('pagetitle','Firma Ekleme / Düzenleme');
		$this->set('content','addcustomer');

		$cid = $this->get("PARAMS.cid");

		$customer = new Axon("customers");
		$customer->load(array('ID=:cid',array(':cid'=>$cid)));
		//die(print_r($customer->name));
		$this->set("customer",$customer);
		$this->set("cities",$this->getCities());
		$this->set("sectors",$this->getSectors());
		$this->set("members",$this->getMembers());
		$this->set("customerTypes",$this->getCustomerTypes());
	}

	function saveCustomer(){
		$this->hasPermission($this->get("PermissionTypes.editFirm"));

		$customer = new Axon("customers");
		$customer->load(
			array(
				'ID=:cid',
				array(
					':cid'=>intval($this->get('POST.ID'))
				)
			)
		);
		$customer->copyFrom('POST');
		$customer->save();

		$customer=new Axon('customers');
		$customer->def('newCustomerID','MAX(ID)');
		$customer->load();
		die(json_encode(array("error"=>0,"newCustomerID"=>$customer->newCustomerID)));
	}

	function deleteCustomer(){
		$this->hasPermission($this->get("PermissionTypes.deleteFirm"));

		$cid = intval(F3::get('POST.customerID'));
		DB::sql(
			array(
				"DELETE FROM notes WHERE companyID=" . $cid,
				"DELETE FROM status WHERE company=" . $cid,
				"DELETE FROM customers WHERE ID=" . $cid
			)
		);
		die(json_encode(array("error"=>0)));
	}

	function addStatus(){
		$this->hasPermission($this->get("PermissionTypes.changeStatus"));

		require_once 'upload.php';
		$upload_handler = new UploadHandler();

		$info = $upload_handler->post();

		F3::set('POST',F3::scrub($_POST));

		$nstatus = new Axon("status");
		$nstatus->status = $this->get('POST.status');
		$nstatus->company = $this->get('POST.company');
		$nstatus->date = $this->get('POST.date');
		$nstatus->manager = $this->get('SESSION.accID');
		$nstatus->note = $this->get('POST.note');
		$nstatus->file = (isset($info[0]->name) && strlen($info[0]->name)>0 ? $info[0]->name : "");
		$nstatus->campaign = $this->get('POST.campaign');
		$nstatus->save();

		$nstatus=new Axon('status');
		$nstatus->def('newStatusID','MAX(ID)');
		$nstatus->load(array('company=:cid',array(':cid'=>$this->get('POST.company'))));

		//die(json_encode(array("error"=>0)));
		F3::reroute("/customerDetail/".$this->get('POST.company')."/#s".$nstatus->newStatusID);
	}

	function addNote(){
		$this->hasPermission($this->get("PermissionTypes.editFirm"));

		F3::set('POST',F3::scrub($_POST));

		$notes = new Axon("notes");
		$notes->companyID = $this->get('POST.company');
		$notes->note = $this->get('POST.note');
		$notes->date = time();
		$notes->noted = $this->get('SESSION.userid');
		$notes->save();

		$notes=new Axon('notes');
		$notes->def('newNoteID','MAX(ID)');
		$notes->load(array('companyID=:cid',array(':cid'=>$this->get('POST.company'))));

		//die(json_encode(array("error"=>0)));
		F3::reroute("/customerDetail/".$this->get('POST.company')."/#n".$notes->newNoteID);
	}

	function deleteStatus(){
		$this->hasPermission($this->get("PermissionTypes.deleteStatus"));

		$statusID = intval(F3::get('POST.statusID'));
		DB::sql("DELETE FROM status WHERE `ID`=" . $statusID . " AND manager=" . $this->get("SESSION.accID"));
		die(json_encode(array("error"=>0)));
	}

	function deleteNote(){
		$this->hasPermission($this->get("PermissionTypes.editFirm"));

		$noteID = intval(F3::get('POST.noteID'));
		DB::sql("DELETE FROM notes WHERE `ID`=" . $noteID . " AND noted=" . $this->get("SESSION.userid"));
		die(json_encode(array("error"=>0)));
	}

	function addSector(){
		$this->hasPermission($this->get("PermissionTypes.addSector"));

		$this->set('pagetitle','Sektör Ekleme');
		$this->set('content','addsector');
	}

	function saveSector(){
		$this->hasPermission($this->get("PermissionTypes.addSector"));

		$sector = new Axon("sector");
		$sector->copyFrom('POST');
		$sector->save();
		die(json_encode(array("error"=>0)));
	}

	function addCustomerType(){
		$this->hasPermission($this->get("PermissionTypes.admin"));

		$this->set('pagetitle','Firma Tipi Ekleme');
		$this->set('content','addcustomertype');
	}

	function saveCustomerType(){
		$this->hasPermission($this->get("PermissionTypes.admin"));

		$customerType = new Axon("customertype");
		$customerType->copyFrom('POST');
		$customerType->save();
		die(json_encode(array("error"=>0)));
	}
}
?>
