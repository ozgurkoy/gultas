<?php
class CrmPublisherSettings extends Crm {
	function Lists(){
		$this->hasPermission($this->get("PermissionTypes.publishers"));

		$this->set('pagetitle','Publishers List');
		$this->set('content','publishersLists');
                
                
                $this->set('UserLists',  CrmCommon::GetAllUserLists());
                

	}
        
        
        function ListManage(){
             $this->hasPermission($this->get("PermissionTypes.addPublisher"));
             try {
                 
                $action = F3::get('POST.action');
                $value = F3::get('POST.data');
                
                
                $PublisherLists = new Axon("PublisherLists");
                $List_To_Users  = new Axon("List_To_Users");
                
                
                switch ($action) {
                    case "create":
                    {
                        $PublisherLists->ListName = $value;
                        $PublisherLists->Owner = $this->get("SESSION.accID");
                        $PublisherLists->save();
                        die(json_encode(array("error"=>0,"value"=>$PublisherLists->ListName,"ID"=>$PublisherLists->_id)));
                        break;
                    }
                    case "update":
                    {
                        $PublisherLists->load(
			array(
				'ID=:cid',
				array(
					':cid'=>$value
				)
			)
                        );
                        break;
                    }
                    case "delete":
                    {
                        break;
                    }
                    default:
                        break;
                }
                
                die(json_encode(array("error"=>1,"msg"=>"error saving")));
                 
             } catch (Exception $exc) {
                 echo $exc->getTraceAsString();
             }
        }
        
        
        function TagDelete()
        {
            $this->hasPermission($this->get("PermissionTypes.addPublisher"));
             try {
                 
                $tagID = F3::get('POST.id'); 
                $Tag = new Axon("TagList");
                $UsersTags = new Axon("UsersTags");
                $UsersTags->erase("TagID=".$tagID);
                $Tag->erase('ID='.$tagID);
                
                // remove other connections
                
                } 
                catch (Exception $exc) {
                 echo $exc->getTraceAsString();
             }
             
        }
        
        function updateUserTags()
        {
            
                $userID = $this->get("GET.id");
                $publisherTags = F3::get('POST.PublisherTags'); 
                
                $UsersTags = new Axon("UsersTags");
                $UsersTags->erase('UserID='.$userID);

                foreach ($publisherTags as $key => $value) {
                    $UsersTags->UserID = $userID;
                    $UsersTags->TagID = $value;
                    $UsersTags->save();
                }
                
                die('ok');
        }
        
        
        function TagSave(){
             $this->hasPermission($this->get("PermissionTypes.addPublisher"));
             try {
                 
                $value = F3::get('POST.value'); 
                $Tag = new Axon("TagList");
                
                 if(F3::get('POST.newTag'))
                 {
                     $group_id = F3::get('POST.group');
                 }  else {
                     $Tag_ids = explode("_", F3::get('POST.id'));
                     //$Tag->afindone("ID=$Tag_ids[1]");
                     $group_id = $Tag_ids[0];
                     
                     
                     $Tag->load(
			array(
				'ID=:cid',
				array(
					':cid'=>intval($Tag_ids[1])
				)
			)
                    ); 
                 }
                 
                 $Tag->typeName = $value;
                 $Tag->GroupID = $group_id;
                 $Tag->save();
                 
                 if(F3::get('POST.newTag'))
                 {
                     die(json_encode(array("error"=>0,"value"=>$value,"GroupID"=>$group_id)));
                 }  else {
                     die($value);
                 }
                 
             } catch (Exception $exc) {
                 echo $exc->getTraceAsString();
             }
        }
        
        function Tags(){
		$this->hasPermission($this->get("PermissionTypes.addPublisher"));

		$this->set('pagetitle','Tags');
		$this->set('content','Tags');
                
		$Groups = CrmCommon::GetAllUserGroups();
		$TagGroups = CrmCommon::GetAllTags();
                
		$this->set("Groups",$Groups);
                $this->set("TagGroups",$TagGroups);
		
	}
        
        function UserLog(){
		$this->hasPermission($this->get("PermissionTypes.addPublisher"));

		$this->set('pagetitle','Activity Log');
		$this->set('content','UserLog');
                
		$this->set("Logs",  CrmCommon::GetUserLog());
                
		
	}

	function deletePublisher(){
		$this->hasPermission($this->get("PermissionTypes.deletePublisher"));

		$twitterid = F3::get('POST.twitterid');
		$user = new Axon("users");
		$user->load(array('twitterid=:uid',array(':uid'=>$twitterid)));
		$user->erase();
		$userStats = new Axon("numbers");
		$userStats->load(array('userID=:uid',array(':uid'=>$twitterid)));
		$userStats->erase();
		die(json_encode(array("error"=>0)));
	}
}
?>
