var checkIntervals= new Array();

var setIntervalForCheck = function(o,p){
	checkIntervals[o.attr("username")] = setInterval(function(){
		calculate(o,p,null,true);
	},5000);
};

var showMsg = function(msg,nowarning){
	if (!nowarning){
		$.msg(msg);
	} else {
		if (window.console){
			console.log(msg);
		}
	}
}

var calculate = function(o,p,recalculate,nowarning){
	$.ajax({
		type: "POST",
		url: "/savePublisher",
		data: { username: o.attr("username"), update:'1', forcedCalc:recalculate},
		dataType: "json",
		success:function(data){
			if (data.error != 0){
				if (data.error == "finishing" || data.error == "calculating" || data.error == "getting" || data.error == "reseted"){
					if (data.error == "getting") data.error = "Getting data from Twitter";
					if (data.error == "calculating") data.error = "Calculating Numbers";
					if (data.error == "finishing") data.error = "Finishing";
					if (data.error == "reseted"){
						data.error = "Resetted";
						if (data.detail == "getting") data.detail = "Getting data from Twitter";
						if (data.detail == "calculating") data.detail = "Calculating numbers";
						if (data.detail == "finishing") data.detail = "Finishing";
					}
					$(".v-follower",p).html(data.error);
					$(".v-follower-date",p).html(data.error);
					$(".v-influence",p).html(data.error);
					$(".v-cost",p).html(data.error);
					$(".v-cost-date",p).html(data.error);
					if (data.error == "Resetted"){
						showMsg("Calculation process stucked at '" + data.detail + "' state. So It's restarted",nowarning);
					} else {
						showMsg(data.error,nowarning);
					}
					if (!checkIntervals[o.attr("username")]){
						setIntervalForCheck(o,p);
					}
				} else {
					showMsg("An error occurred when performing your action. Please try again later.",nowarning);
				}
			} else {
				$(".v-follower",p).html(data.followers_count);
				$(".v-follower-date",p).html(data.followerUpdateDate);
				$(".v-influence",p).html(data.true_reach);
				$(".v-cost",p).html(data.cost);
				$(".v-cost-date",p).html(data.costUpdateDate);


				if (!nowarning){
					if (data.cost != "Calculating" && recalculate == null){
						$.confirm(
							"Cost calculated " + data.costUpdateDateDifference + " ago. Do you want to calculate again?",
							function(){
								calculate(o,p,1,false);
							},
							function(){
								$.msg("The user information was successfully updated.");
							}
						);
					} else {
						showMsg("The user information was successfully updated.",nowarning);
					}
				}
				if (data.cost != "Calculating"){
					if (checkIntervals[o.attr("username")]){
						clearInterval(checkIntervals[o.attr("username")]);
						checkIntervals[o.attr("username")] = null;
					}
				} else {
					if (!checkIntervals[o.attr("username")]){
						setIntervalForCheck(o,p);
					}
				}
			}

		}
	});
}