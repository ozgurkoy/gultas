<?php
include("inc/config/config.php");
?>
<!doctype html>
<html lang="tr-tr">
<head>
	<?php include("inc/meta.php");?>
</head>
<body>
			<?php include("inc/headnav.php");?>
		<header>
			<?php include("inc/header.php"); ?>
		</header>
		<nav>
			<?php include("inc/sidebar.php"); ?>
		</nav>
		<section id="content">
			<div class="g12 widgets">
				
				<div class="widget" id="calendar_widget" data-icon="calendar">
					<h3 class="handle">Profil Güncelleme Değişikliği</h3>
					<div>
						<?php
							if($_REQUEST["a"] == "b")
							{
								echo "<div class='alert success'>Profil güncelleme başarılı</div>";
							}
						?>
								
						<?php
							$username = $_SESSION['userid'];
							$sql = "SELECT * FROM `members` WHERE username='".$username."'";
						    $row = $dbh->query($sql)->fetch();
						?>	
						<form action="editProfile.php?edit=profile" method="post" id="form"  data-ajax="false">
							<fieldset>
								
								<section><label for="text_field">Ad:</label>
									<div><input type="text" id="name" name="name" value="<?php echo $row['name']; ?>"></div>
								</section>
								<section><label for="text_field">Soyad:</label>
									<div><input type="text" id="surname" name="surname" value="<?php echo $row['surname']; ?>"></div>
								</section>
								<section><label for="text_field">E-posta:</label>
									<div><input type="text" id="email" name="email" value="<?php echo $row['email']; ?>"></div>
								</section>
								<section><label for="text_field">GSM:</label>
									<div><input type="text" id="gsm" name="gsm" value="<?php echo $row['gsm']; ?>"></div>
								</section>
								<section><label for="file_upload">Fotoğraf<br><span>Sistem profil fotografinizi <a href="https://tr.gravatar.com/" target="_blank">Gravatar</a> ile eslestirmektedir.</span></label>
									<div>
									<?php
									$grav_url = "http://www.gravatar.com/avatar/" . md5( strtolower( trim( $row['email'] ) ) ) . "?d=" . urlencode( $default ) . "&s=" . $size;
									?>
										<img src="<?php echo $grav_url; ?>" alt="" />
			
									</div>
								</section>
								<section>
									<div><button class="updateProfile submit" name="submitbuttonname" value="submitbuttonvalue">Güncelle</button></div>
								</section>
							</fieldset>
						</form>
					</div>
				</div>
				
				
				
				<div class="widget" id="calendar_widget" data-icon="calendar">
					<h3 class="handle">Şifre Değişikliği</h3>
					<div>
						<?php
							if($_REQUEST["a"] == "a")
							{
								echo "<div class='alert success'>Şifre değişikliği başarılı</div>";
							}
						?>
								
							
						<form action="editProfile.php?edit=password" method="post" id="form"  data-ajax="false">
							<fieldset>
								
								<section><label for="text_field">Yeni Şifre:</label>
									<div><input type="password" id="password" class="password" name="password"></div>
								</section>
								<section>
									<div><button class="updatePassword submit" name="submitbuttonname" value="submitbuttonvalue">Düzenle</button></div>
								</section>
							</fieldset>
						</form>
					</div>
				</div>



				
			</div>
					
		</section>
		<footer><?php include("inc/footer.php"); ?></footer>
</body>
</html>