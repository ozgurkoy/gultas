<?php
include("inc/config/config.php");
if (!checkPermission(PermissionTypes::firms)) header('Location: index.php');
?>
<!doctype html>
<html lang="tr-tr">
<head>
	<?php include("inc/meta.php");?>
	<script type="text/javascript">
		$(document).ready(function(){
			$(".i_trashcan").live("click",function(){
				if (confirm("Notu silmek istediğinize emin misiniz?")){
					var o = $(this);
					$.get("customerFunctions.php",{del : "2", noteID : o.attr("noteID")},function(data){
						if (data.error == 0){
							o.parent().parent().fadeOut();
						}
					},"json");
				}
				return false;
			});
		});
	</script>
</head>
<body>
			<?php include("inc/headnav.php");?>
		<header>
			<?php include("inc/header.php"); ?>
		</header>
		<nav>
			<?php include("inc/sidebar.php"); ?>
		</nav>
		<section id="content">

			<div class="g12">
				<script>

					$(document).ready(function() {
						var $content = $('#content');
						$content.find("table.akilliNotlar").dataTable({
							"sPaginationType": "full_numbers"
						});

					});
				</script>
							<!-- last notes -->
							<div class="widget" id="newfirm" data-icon="calendar">
								<h3 class="handle">Notlar</h3>
								<div>

										<table class="akilliNotlar">
										<thead>
											<tr>
												<th width="100">Tarih</th>
												<th>Firma</th>
												<th>Not</th>
												<th>Ekleyen</th>
												<th>&nbsp;</th>
											</tr>
										</thead>
										<tbody>
											<?php
												$sql = 'SELECT * , DATE( FROM_UNIXTIME( date ) ) AS eklemeTarihi FROM  `notes` ORDER BY date DESC';
											    foreach ($dbh->query($sql) as $row):
												$dater = $row['date'];
												$convertDate  = strtotime(date('Y-m-d 00:00:00', $dater));
												$NewDate = date("F jS, Y", strtotime($dater))
											?>

											<tr class="gradeA" id="note<?php echo $row['ID']; ?>">

												<td><?php echo $row['eklemeTarihi']; ?></td>
												<td>
													<?php
														$sql = 'SELECT * FROM customers WHERE  ID='. $row['companyID'];
													    $rowC = $dbh->query($sql)->fetch();
													?>
												<a href="<?php echo $row['companyID']; ?>.adm"><?php echo $rowC['name']; ?></a></td>
												<td style="text-align:left;"><?php echo $row['note']; ?></td>
												<td><?php echo $row['noted']; ?></td>
												<td>
													<?php if (checkPermission(PermissionTypes::editFirm) && $_SESSION['userid'] == $row['noted']): ?>
													<a href="#" class="btn i_trashcan red" title="Sil" noteID="<?php echo $row['ID']; ?>"></a>
													<?php endif; ?>
												</td>
											</tr>
											<?php endforeach; ?>
										</tbody>
										</table>


								</div>
							</div>
							<!-- end -->


			</div>

		</section>
		<footer><?php include("inc/footer.php"); ?></footer>
</body>
</html>