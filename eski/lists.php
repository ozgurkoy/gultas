<?php
include("inc/config/config.php");
if (!checkPermission(PermissionTypes::publishers)) header('Location: index.php');

?>
<!doctype html>
<html lang="tr-tr">
<head>
	<?php include("inc/meta.php");?>
</head>
<body>
			<?php include("inc/headnav.php");?>
		<header>
			<?php include("inc/header.php"); ?>
		</header>
		<nav>
			<?php include("inc/sidebar.php"); ?>
		</nav>
		<section id="content">
			<div class="g12 widgets">
				<?php if (checkPermission(PermissionTypes::addPublisher)): ?>
				<div class="widget" id="calendar_widget" data-icon="calendar">
					<h3 class="handle">Yeni Kullanıcı Ekle</h3>
					<div>
						<form action="functions.php" method="post" id="form">
							<fieldset>
								<section><label for="text_field">Kullanıcı Adı:</label>
									<div><input type="text" id="text_field" name="twitterUserName"></div>
								</section>
								<section>
									<label for="dropdown_vegetables">Kategori</label>
									<div>
										<select name="type" id="dropdown_fruits">
											<?php
												$sqlx = 'SELECT * FROM userType';
											    foreach ($dbh->query($sqlx) as $rowx):
											?>
												<option value="<?php echo $rowx['ID']; ?>" /><?php echo $rowx['type']; ?></option>
												<?php endforeach; ?>
										</select>
									</div>
								</section>
								<section>
									<div><button class="newTwitterHandle submit" name="submitbuttonname" value="submitbuttonvalue">Ekle</button></div>
								</section>
							</fieldset>
						</form>
					</div>
				</div>
				<?php endif; ?>
				
				<div class="widget" data-icon="number" id="widget_ajax" data-load="list.php?tID=<?php echo $_REQUEST['tID']; ?>" data-reload="600" data-remove-content="false">
					<h3 class="handle">Tüm Takip Edilenler</h3>
					<div>
						Ajax ile güncelleniyor
					</div>
				</div>
			</div>

		</section>
		<footer><?php include("inc/footer.php"); ?></footer>
</body>
</html>