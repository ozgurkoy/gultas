<?php
include("inc/config/config.php");
?>
<!doctype html>
<html lang="tr-tr">
<head>
	<?php include("inc/meta.php");?>
	<script type="text/javascript">
		$(document).ready(function(){
			$(".btn-note-delete").live("click",function(){
				if (confirm("Notu silmek istediğinize emin misiniz?")){
					var o = $(this);
					$.get("customerFunctions.php",{del : "2", noteID : o.attr("noteID")},function(data){
						if (data.error == 0){
							o.parent().parent().fadeOut();
						}
					},"json");
				}
				return false;
			});
			$(".btn-status-delete").live("click",function(){
				if (confirm("Durumu silmek istediğinize emin misiniz?")){
					var o = $(this);
					$.get("customerFunctions.php",{del : "3", statusID : o.attr("statusID")},function(data){
						if (data.error == 0){
							o.parent().parent().fadeOut();
						}
					},"json");
				}
				return false;
			});
		});
	</script>
</head>
<body>
			<?php include("inc/headnav.php");?>
		<header>
			<?php include("inc/header.php"); ?>
		</header>
		<nav>
			<?php include("inc/sidebar.php"); ?>
		</nav>
		<section id="content">
			<script>
				$(function() {
					$(".meter > span").each(function() {
						$(this)
							.data("origWidth", $(this).width())
							.width(0)
							.animate({
								width: $(this).data("origWidth")
							}, 1200);
					});
				});
			</script>

			<?php
			$company = $_REQUEST["firmID"];
			$sql = "SELECT
						c.*,
						s.name AS sectorName,
						ct.name AS typeName,
						status.date AS lastStatusDate,
						st.type AS lastStatusType,
						st.progress AS progressSize
					FROM
						customers c
						LEFT JOIN sector s ON s.ID = c.sectorID
						LEFT JOIN customertype ct ON ct.ID = c.type
						LEFT JOIN status ON status.company = c.ID
						LEFT JOIN statusType st ON st.ID = status.status
					WHERE
						c.ID=". $company ."
					ORDER BY status.date DESC
					LIMIT 0,1
					";
			$row = $dbh->query($sql)->fetch();

			if ($row["progressSize"] == '')
			{
				$display = 'none';
			}

			?>
			<div class="meter" style="display:<?php echo $display; ?>;">
				<span style="width: <?php echo $row["progressSize"]; ?>%">
					<?php echo $row["lastStatusType"]; ?> - <?php echo $row["lastStatusDate"]; ?>
				</span>
			</div>

				<div class="widget" id="firmDetails" data-icon="calendar">
					<h3 class="handle">Firma Detayları</h3>
					<div>
						<form id="formFirmAdd"  data-ajax="false">
							<fieldset>
								<section><label for="text_field">Firma :</label>
									<div><?php echo $row['name']; ?></div>
								</section>
								<section><label for="text_field">Yetkili Kişi :</label>
									<div><?php echo $row['responsiblehead']; ?></div>
								</section>
								<section><label for="text_field">Adres :</label>
									<div><?php echo $row['location']; ?></div>
								</section>
								<section>
									<label for="customer_type">Şehir</label>
									<div>
										<?php echo $row['city']; ?>
									</div>
								</section>
								<section><label for="text_field">Telefon :</label>
									<div><?php echo $row['phone']; ?></div>
								</section>
								<section><label for="text_field">GSM :</label>
									<div><?php echo $row['gsm']; ?></div>
								</section>
									<section>
										<label for="customer_sector">Sektör</label>
										<div>
											<?php echo $row['sectorName']; ?>
										</div>
									</section>
									<section>
										<label for="customer_type">Tip</label>
										<div>
											<?php echo $row['typeName']; ?>
										</div>
									</section>

							</fieldset>
						</form>
						<?php if (checkPermission(PermissionTypes::editFirm)): ?>
						<a href="admin_editFirm.php?firmID=<?php echo $_REQUEST["firmID"]; ?>" class="btn i_pencil icon yellow">Firmayı Düzenle</a>
						<?php endif; ?>
					</div>
				</div>

				<?php if (checkPermission(PermissionTypes::changeStatus)): ?>
				<div class="widget" id="addStatus" data-icon="calendar">
					<h3 class="handle">Yeni Durum Ekle</h3>
					<div>
						<?php
						//ID, status, company, date, manager, note, file, file2

						?>

						<form action="customerFunctions.php?add=6" method="post" id="status"  data-ajax="false" enctype="multipart/form-data" >
							<fieldset>

								<input type="hidden" id="noted" name="manager" value="<?php echo $_SESSION['accID']; ?>">
								<input type="hidden" id="companyID" name="company" value="<?php echo $_REQUEST["firmID"]; ?>">


								<section>
									<label for="statusType">Durum</label>
									<div>
										<select name="status" id="statuses">
											<option/>Seçin</option>
											<?php
												$sqlx = 'SELECT * FROM statusType';
											    foreach ($dbh->query($sqlx) as $rowx):
											?>
												<option value="<?php echo $rowx['ID']; ?>" /><?php echo $rowx['type']; ?></option>
												<?php endforeach; ?>
										</select>
									</div>
								</section>

								<section>
									<label for="statusType">İlgili Proje</label>
									<div>
										<select name="campaign" id="campaign">
											<option/>Seçin</option>
											<?php
												$sqlx = "SELECT id, CONCAT_WS(' ',campCode,title) AS title FROM campaign WHERE crmmember=" . $_SESSION['accID'] . " ORDER BY title";
											    foreach ($dba->query($sqlx) as $rowx):
											?>
												<option value="<?php echo $rowx['id']; ?>" /><?php echo $rowx['title']; ?></option>
												<?php endforeach; ?>
										</select>
									</div>
								</section>



								<section><label for="textarea">Tarih</label>
									<div><input id="date" name="date" type="text" class="date"></div>
								</section>
								<section><label for="textarea">Not</label>
									<div><textarea id="textarea" name="note" rows="5"></textarea></div>
								</section>
								<section><label for="file_upload">Dosya<br><span>Durum ile ilgili dosya varsa lütfen yükleyin. (teklif, sözleşme v.b)</span></label>
									<div><input type="file" id="file_upload" name="file_upload" data-auto-upload="false" multiple data-allowed-extensions="[pdf,doc,docx,ppt,pptx,txt,zip,rar]" data-max-file-size="10000000"></div>
								</section>
								<section>
									<div><button class="newStatusForFirm submit green" name="submitbuttonname" value="submitbuttonvalue">Durum Ekle</button></div>
								</section>
							</fieldset>
						</form>


					</div>
				</div>
				<?php endif; ?>

				<div class="widget" id="statusChanges" data-icon="calendar">
					<h3 class="handle">Durum Degisiklikleri</h3>
					<div>

							<table class="dataTable">
							<thead>
								<tr>
									<th>Tarih</th>
									<th>Durum</th>
									<th>İlgili Kampanya</th>
									<th>Not</th>
									<th>Dosya</th>
									<th>Duzenleyen</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<?php
									$sql = "
										SELECT
											s.ID,
											s.date,
											s.note,
											s.file,
											s.manager,
											s.campaign,
											st.type AS statusType,
											CONCAT_WS(' ',m.name,m.surname) AS manager_name,
											m.username AS manager_username
										FROM `status` s
											INNER JOIN statusType st ON st.ID = s.status
											INNER JOIN members m ON m.ID = s.manager
										WHERE company='".$company."'
										ORDER BY date DESC
									";
									foreach ($dbh->query($sql) as $row):
								?>

								<tr class="gradeA">

									<td><?php echo $row['date']; ?></td>
									<td><?php echo $row['statusType'];; ?></td>
									<td>
									<?php
										if (strlen($row["campaign"])>0 && $row["campaign"] != 0){
										$sqlx = "SELECT CONCAT_WS(' ',campCode,title) AS title FROM campaign WHERE id=" . $row["campaign"];

										$rowc = $dba->query($sqlx)->fetch();
											echo $rowc["title"];
										}else{
											echo "-";
										}
									?>
									</td>
									<td><?php echo $row['note']; ?></td>
									<td><a href="./upload/<?php echo $row['file']; ?>"><?php echo $row['file']; ?></a></td>
									<td><a href="/username/<?php echo $row['manager_username']; ?>"><?php echo $row['manager_name']; ?></a></td>
									<td>
										<?php if (checkPermission(PermissionTypes::deleteStatus) && $row["manager"] == $_SESSION["accID"]): ?>
										<a href="#" class="btn-status-delete btn i_trashcan red" title="Sil" statusID="<?php echo $row['ID']; ?>"></a>
										<?php endif; ?>
									</td>
								</tr>
								<?php endforeach; ?>
							</tbody>
							</table>


					</div>
				</div>

				<?php if (checkPermission(PermissionTypes::editFirm)): ?>
				<div class="widget" id="newfirm" data-icon="calendar">
					<h3 class="handle">Yeni Not Ekle</h3>
					<div>

						<form action="customerFunctions.php?add=5" method="post" id="firmNotes"  data-ajax="false">
							<fieldset>
								<input type="hidden" id="companyID" name="firmID" value="<?php echo $_REQUEST["firmID"]; ?>">
								<section><label for="textarea">Not</label>
									<div><textarea id="textarea" name="note" rows="3"></textarea></div>
								</section>
								<section>
									<div><button class="newNoteForFirm submit green" name="submitbuttonname" value="submitbuttonvalue">Not Ekle</button></div>
								</section>
							</fieldset>
						</form>


					</div>
				</div>
				<?php endif; ?>

				<div class="widget" id="newfirm" data-icon="calendar">
					<h3 class="handle">Notlar</h3>
					<div>
						<table class="dataTable">
						<thead>
							<tr>
								<th width="100">Tarih</th>
								<th>Not</th>
								<th>Ekleyen</th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$sql = 'SELECT *, DATE( FROM_UNIXTIME( date ) ) AS eklemeTarihi FROM notes WHERE companyID='. $_REQUEST["firmID"];
								foreach ($dbh->query($sql) as $row):
								$dater = $row['date'];
								$convertDate  = strtotime(date('Y-m-d 00:00:00', $dater));

							?>

							<tr class="gradeA" id="note<?php echo $row['ID']; ?>">

								<td><?php echo $row['eklemeTarihi']; ?></td>
								<td style="text-align:left;"><?php echo $row['note']; ?></td>
								<td><a href="username/<?php echo $row['noted']; ?>"><?php echo $row['noted']; ?></a></td>
								<td>
									<?php if (checkPermission(PermissionTypes::editFirm) && $_SESSION['userid'] == $row['noted']): ?>
									<a href="#" class="btn-note-delete btn i_trashcan red" title="Sil" noteID="<?php echo $row['ID']; ?>"></a>
									<?php endif; ?>
								</td>
							</tr>
							<?php endforeach; ?>
						</tbody>
						</table>
					</div>
				</div>
		</section>
		<footer><?php include("inc/footer.php"); ?></footer>
</body>
</html>