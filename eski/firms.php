<?php
include("inc/config/config.php");
if (!checkPermission(PermissionTypes::firms)) header('Location: index.php');

?>
<!doctype html>
<html lang="tr-tr">
<head>
	<?php include("inc/meta.php");?>
</head>
<body>
			<?php include("inc/headnav.php");?>
		<header>
			<?php include("inc/header.php"); ?>
		</header>
		<nav>
			<?php include("inc/sidebar.php"); ?>
		</nav>
		<section id="content">
<div class="g12 widgets">
					<div class="widget" data-icon="number" id="widget_ajax" data-load="firm.php?tID=<?php echo $_REQUEST['tID']; ?>&v=<?php echo $_REQUEST['v']; ?>" data-reload="600" data-remove-content="false">
						<h3 class="handle">Firmalar</h3>
						<div>
							Ajax ile güncelleniyor
						</div>
					</div>
			</div>

		</section>
		<footer><?php include("inc/footer.php"); ?></footer>
</body>
</html>