<?php
include("inc/config/config.php");
if (!checkPermission(PermissionTypes::admin)) header('Location: index.php');

if (isset($_POST["w"])){
	$w = $_POST["w"];
	$error = false;
	switch ($w){
		case "getUserPermissions":
			$u = $_POST["u"];
			if (is_numeric($u)){
				$s = "SELECT * FROM permissions WHERE member = $u";
				$r[] = $dbh->query($s)->fetchAll();
				echo json_encode(array("error" => "0", "result" => $r));
			}else{
				$error = true;
			}
			break;
		case "updateUserPermissions":
			$u = $_POST["u"];
			$p = $_POST["p"];
			$a = $_POST["a"];
			if (is_numeric($u) && is_numeric($p)){
				if (strtolower($a) == "checked")
					$s = "INSERT IGNORE INTO permissions(member,module) VALUES($u,$p);";
				else
					$s = "DELETE FROM permissions WHERE member = $u AND module = $p;";
				$dbh->query($s);
				echo json_encode(array("error" => "0", "result" => "ok"));
			}else{
				$error = true;
			}
			break;
	}
	if ($error)
		die(json_encode(array("error"=>"1")));
	else
		die();
}
?>
<!doctype html>
<html lang="tr-tr">
<head>
	<?php include("inc/meta.php");?>
	<script type="text/javascript">
		$(document).ready(function(){
			$("#users").change(function(){
				t = $(this);
				if (t.val().length>0){
					$.post(document.URL,{w : "getUserPermissions", u : t.val()},function(data){
						$(".permission_check").attr("disabled",false);
						$("#s_permissions").slideDown();
						$(".permission_check").attr("checked",false);
						if (data.error == 0){
							for (i=0;i<data.result[0].length;i++){
								p = data.result[0][i];
								$("#" + p.module + "_check").attr("checked",true);
							}
						}
					},"json");
				} else {
					$("#s_permissions").slideUp();
					$(".permission_check").attr("checked",false);
					$(".permission_check").attr("disabled",true);
				}
			});
			$(".permission_check").change(function(){
				t = $(this);
				if ($("#users").val().length>0){
					$.post(document.URL,{w : "updateUserPermissions", u : $("#users").val(), p : t.val(), a : t.attr("checked")},function(data){
						if (data.error == 0){
							$.msg("Yetki güncellendi.");
						}
					},"json");
				}else {
					return false;
				}
			});
		});
	</script>
	<style type="text/css">
		#s_permissions {display:none}
	</style>
</head>
<body>
			<?php include("inc/headnav.php");?>
		<header>
			<?php include("inc/header.php"); ?>
		</header>
		<nav>
			<?php include("inc/sidebar.php"); ?>
		</nav>
		<section id="content">
			<div class="g12 widgets">




				<div class="widget" id="newfirm" data-icon="calendar">
					<h3 class="handle">Yetkiler</h3>
					<div>
						<form action="customerFunctions.php?add=3" method="post" id="formFirmAdd"  data-ajax="false">
							<fieldset>


								<section>
									<label for="dropdown_vegetables">Yetkilendirilecek Kullanıcı</label>
									<div>

										<select name="users" id="users">
											<optgroup label="Kullanıcılar">
												<option value="">Seçin</option>
												<?php
													$sqlx = 'SELECT * FROM members ORDER BY id ASC';
													foreach ($dbh->query($sqlx) as $rowx):
												?>
												<option value="<?php echo $rowx['id']; ?>"><?php echo $rowx['name'] . " " . $rowx['surname']; ?> </option>
													<?php endforeach; ?>
											</optgroup>
										</select>
									</div>
								</section>
								<section id="s_permissions">
									<label>Yetkili Alanlar</label>
									<div>

										<?php
											$sqlx = 'SELECT * FROM modules ORDER BY display ASC';
										    foreach ($dbh->query($sqlx) as $rowx):
										?>
										<input type="checkbox"  value="<?php echo $rowx['ID']; ?>"  id="<?php echo $rowx['ID']; ?>_check" class="permission_check" disabled="true"><label for="<?php echo $rowx['ID']; ?>_check"><?php echo $rowx['display']; ?></label><br />
											<?php endforeach; ?>


									</div>
								</section>
							</fieldset>
						</form>
					</div>
				</div>


			</div>

		</section>
		<footer><?php include("inc/footer.php"); ?></footer>
</body>
</html>