<?php
include("inc/config/config.php");
if (!checkPermission(PermissionTypes::admin)) header('Location: index.php');
?>
<!doctype html>
<html lang="tr-tr">
<head>
	<?php include("inc/meta.php");?>
</head>
<body>
			<?php include("inc/headnav.php");?>
		<header>
			<?php include("inc/header.php"); ?>
		</header>
		<nav>
			<?php include("inc/sidebar.php"); ?>
		</nav>
		<section id="content">
			<div class="g12 widgets">
				<div class="widget" id="calendar_widget" data-icon="calendar">
					<h3 class="handle">Yeni Yetkili Ekle</h3>
					<div>
						<form action="register.php" method="post" id="form"  data-ajax="false">
							<fieldset>
								<section><label for="text_field">Kullanıcı Adı:</label>
									<div><input type="text" id="text_field" name="username"></div>
								</section>
								<section><label for="text_field">Şifre:</label>
									<div><input type="password" id="password" class="password" name="password"></div>
								</section>
								<section>
									<div><button class="newAdminUser submit" name="submitbuttonname" value="submitbuttonvalue">Ekle</button></div>
								</section>
							</fieldset>
						</form>
					</div>
				</div>


			</div>

		</section>
		<footer><?php include("inc/footer.php"); ?></footer>
</body>
</html>