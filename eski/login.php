<?php
include("inc/config/config.php");

?>
<!doctype html>
<html lang="tr-tr">
<head>
	<?php include("inc/meta.php");?>
</head>
<body id="login">
		<header>
			<div id="logo">
				<a href="login.php">adMingle</a>
			</div>
		</header>
		<section id="content">
		<form action="logincheck.php" id="loginform" method="post" data-ajax="false">
			<fieldset>
				<section><label for="username">Kullanıcı adı</label>
					<div><input type="text" id="username" name="username" autofocus></div>
				</section>
				<section><label for="password">Şifre</label>
					<div><input type="password" id="pass" name="password"></div>
					<div><input type="checkbox" id="remember" name="remember"><label for="remember" class="checkbox">beni unutma</label></div>
				</section>
				<section>
					<div><button class="fr submit">Giriş</button></div>
				</section>
			</fieldset>
		</form>
		</section>
		<footer>adMingle: <?php echo $_SESSION['userid']; ?></footer>
		
</body>
</html>