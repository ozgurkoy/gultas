<?php
include("inc/config/config.php");
if (!checkPermission(PermissionTypes::addSector)) header('Location: index.php');
?>
<!doctype html>
<html lang="tr-tr">
<head>
	<?php include("inc/meta.php");?>
</head>
<body>
			<?php include("inc/headnav.php");?>
		<header>
			<?php include("inc/header.php"); ?>
		</header>
		<nav>
			<?php include("inc/sidebar.php"); ?>
		</nav>
		<section id="content">
			<div class="g12 widgets">


				<div class="widget" id="sector" data-icon="calendar">
					<h3 class="handle">Yeni Sektör Ekle</h3>
					<div>
						<form action="customerFunctions.php?add=1" method="post" id="formSector"  data-ajax="false">
							<fieldset>
								<section><label for="text_field">Sektör Adı:</label>
									<div><input type="text" id="text_field" name="name"></div>
								</section>
								<section>
									<div><button class="newCustomerSector submit" name="submitbuttonname" value="submitbuttonvalue">Ekle</button></div>
								</section>
							</fieldset>
						</form>
					</div>
				</div>






			</div>

		</section>
		<footer><?php include("inc/footer.php"); ?></footer>
</body>
</html>