<?php
include("inc/config/config.php");
if (!checkPermission(PermissionTypes::editFirm)) header('Location: index.php');
?>
<!doctype html>
<html lang="tr-tr">
<head>
	<?php include("inc/meta.php");?>
</head>
<body>
			<?php include("inc/headnav.php");?>
		<header>
			<?php include("inc/header.php"); ?>
		</header>
		<nav>
			<?php include("inc/sidebar.php"); ?>
		</nav>
		<section id="content">
			<div class="g12 widgets">
				<div class="widget" id="newfirm" data-icon="calendar">
					<h3 class="handle">Firma Güncelle</h3>
					<div>
						<?php
							$sql = 'SELECT * FROM customers WHERE  ID='. $_REQUEST["firmID"];
						    $row = $dbh->query($sql)->fetch();
						//foreach ($dbh->query($sql) as $row):
						?>
						<form action="customerFunctions.php?add=4&id=<?php echo $row['ID']; ?>" method="post" id="formFirmAdd"  data-ajax="false">
							<input type="hidden" id="companyID" name="firmID" value="<?php echo $_REQUEST["firmID"]; ?>">
							<fieldset>
								<section><label for="text_field">Firma :</label>
									<div><input type="text" id="text_field" name="name" value="<?php echo $row['name']; ?>"></div>
								</section>
								<section><label for="text_field">Yetkili Kişi :</label>
									<div><input type="text" id="text_field" name="responsiblehead"  value="<?php echo $row['responsiblehead']; ?>"></div>
								</section>
								<section><label for="text_field">Adres :</label>
									<div><input type="text" id="text_field" value="<?php echo $row['location']; ?>"  name="location"></div>
								</section>
								<section>
									<label for="customer_type">Şehir</label>
									<div>
										<select name="city" id="cities">
											<option/>Seçin</option>
											<?php
												$sqlx = 'SELECT * FROM cities ORDER BY name ASC';
											    foreach ($dbh->query($sqlx) as $rowx):
											?>
												<option value="<?php echo $rowx['ID']; ?>" <?php echo $rowx['ID'] == $row['city'] ? "selected" : "" ?> /><?php echo $rowx['name']; ?></option>
												<?php endforeach; ?>
										</select>
									</div>
								</section>
								<section><label for="text_field">Telefon :</label>
									<div><input type="text" value="<?php echo $row['phone']; ?>" id="text_field" name="phone"></div>
								</section>
								<section><label for="text_field">GSM :</label>
									<div><input type="text" value="<?php echo $row['gsm']; ?>" id="text_field" name="gsm"></div>
								</section>
									<section>
										<label for="customer_sector">Sektör</label>
										<div>
											<select name="sector" id="sectorsFirm">
												<option/>Seçin</option>
												<?php
													$sqlx = 'SELECT * FROM sector  ORDER BY name ASC';
												    foreach ($dbh->query($sqlx) as $rowx):
												?>
													<option value="<?php echo $rowx['ID']; ?>" <?php echo $rowx['ID'] == $row['sectorID'] ? "selected" : "" ?> /><?php echo $rowx['name']; ?></option>
													<?php endforeach; ?>
											</select>
										</div>
									</section>
									<section>
										<label for="customer_type">Tip</label>
										<div>
											<select name="type" id="typesFirm">
												<option/>Seçin</option>
												<?php
													$sqlx = 'SELECT * FROM customertype  ORDER BY name ASC';
												    foreach ($dbh->query($sqlx) as $rowx):
												?>
													<option value="<?php echo $rowx['ID']; ?>" <?php echo $rowx['ID'] == $row['type'] ? "selected" : "" ?> /><?php echo $rowx['name']; ?></option>
													<?php endforeach; ?>
											</select>
										</div>
									</section>
								<section>
									<div><button class="newCustomerType submit green" name="submitbuttonname" value="submitbuttonvalue">Düzenle</button></div>
								</section>
							</fieldset>
						</form>
							<?php // endforeach; ?>
					</div>
				</div>


			</div>
				<script>
				/*	$(document).ready(function(){
						$('#cities option[value="<?php echo $row['city']; ?>"]').attr("selected", true);
					})*/
				</script>
		</section>
		<footer><?php include("inc/footer.php"); ?></footer>
</body>
</html>