<?php

function logout()
{

	session_start();
	session_destroy();
	$_SESSION = array(); // Clears the $_SESSION variable
	$_SESSION['valid'] = "N";
	setcookie ("admCRM_LU", "", time() - 3600, "/");
	setcookie ("admCRM_LK", "", time() - 3600, "/");
	//echo $_SESSION['valid'];
	header('Location: login.php');
}
logout();

?>