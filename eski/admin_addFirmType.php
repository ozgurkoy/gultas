<?php
include("inc/config/config.php");
if (!checkPermission(PermissionTypes::addFirm)) header('Location: index.php');
?>
<!doctype html>
<html lang="tr-tr">
<head>
	<?php include("inc/meta.php");?>
</head>
<body>
			<?php include("inc/headnav.php");?>
		<header>
			<?php include("inc/header.php"); ?>
		</header>
		<nav>
			<?php include("inc/sidebar.php"); ?>
		</nav>
		<section id="content">
			<div class="g12 widgets">





				<div class="widget" id="firmtype" data-icon="calendar">
					<h3 class="handle">Yeni Firma Tipi Ekle</h3>
					<div>
						<form action="customerFunctions.php?add=2" method="post" id="formFirmType"  data-ajax="false">
							<fieldset>
								<section><label for="text_field">Firma Tipi:</label>
									<div><input type="text" id="text_field" name="name"></div>
								</section>
								<section>
									<div><button class="newCustomerType submit" name="submitbuttonname" value="submitbuttonvalue">Ekle</button></div>
								</section>
							</fieldset>
						</form>
					</div>
				</div>



			</div>

		</section>
		<footer><?php include("inc/footer.php"); ?></footer>
</body>
</html>