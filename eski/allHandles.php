<?php
include("inc/config/config.php");
if (!checkPermission(PermissionTypes::publishers)) header('Location: index.php');

?>
<!doctype html>
<html lang="tr-tr">
<head>
	<?php include("inc/meta.php");?>
</head>
<body>
			<?php include("inc/headnav.php");?>
		<header>
			<?php include("inc/header.php"); ?>
		</header>
		<nav>
			<?php include("inc/sidebar.php"); ?>
		</nav>
		<section id="content">
<div class="g12 widgets">
	<script>

		$(document).ready(function() {
			var $content = $('#content');
			$content.find("table.akilliTablo").dataTable({
				"sPaginationType": "full_numbers"
			});

		});
	</script>
					<div class="widget" data-icon="number" id="widget_ajax">
						<h3 class="handle">Tüm Ünlüler</h3>
						<div>
							<table class="akilliTablo">
							<thead>
								<tr>
											<th>Foto</th>
											<th>Username</th>
											<th>Ad</th>
											<th>Follower</th>
											<th>Tür</th>
											<th>TrueReach</th>
											<th>Klout</th>
											<th>Fiyat</th>
											<th>&nbsp;</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$sql = 'SELECT * FROM users';
								    foreach ($dbh->query($sql) as $row):
								?>

								<tr class="gradeA">
									<td><img src="<?php echo $row['avatar']; ?>" width="50px" height="50px" border="0" /></td>
									<td><a href="http://twitter.com/<?php echo $row['username']; ?>" target="_blank"><?php echo $row['username']; ?></a></td>
									<td><?php echo $row['name']; ?></td>

									<?php
										$sqlSub = 'SELECT * FROM numbers WHERE userID='.$row['twitterid'].' LIMIT 1';
									    foreach ($dbh->query($sqlSub) as $rowSub):
									?>
									<td class="v-follower"><?php echo $rowSub['follower']; ?></td>
									<td class=""><?php echo $row['type']; ?></td>
									<td class="v-influence"><?php echo $rowSub['influence']; ?></td>
									<td class="v-klout"><?php echo $rowSub['klout']; ?></td>
									<td class="v-cost">
									<?php
									$followerCount = $rowSub['follower'];
									$influenceCount = $rowSub['influence'];
									$cost = $influenceCount * 0.002;
									if($influenceCount < $followerCount) {
										$cost += (($followerCount - $influenceCount) * 0.001);
									}
									echo $cost;
									?>
									</td>
									<?php endforeach; ?>
									<td>

										<?php if (checkPermission(PermissionTypes::addPublisher)): ?>
										<a href="functions.php?update=1&twitterUserName=<?php echo $row['username']; ?>" class="btn-update btn i_power yellow" title="Güncelle" username="<?php echo $row['username']; ?>"></a>
										<?php endif; ?>
										<?php if (checkPermission(PermissionTypes::deletePublisher)): ?>
										<a href="delete.php?twitterUserName=<?php echo $row['username']; ?>&id=<?php echo $row['twitterid']; ?>" class="btn-del  btn i_trashcan red" title="Sil" username="<?php echo $row['username']; ?>"></a>
										<?php endif; ?>
									</td>
								</tr>
								<?php endforeach; ?>
							</tbody>
							</table>
						</div>
					</div>
			</div>

		</section>
		<footer><?php include("inc/footer.php"); ?></footer>
</body>
</html>