<?php
	include("inc/config/config.php");
?>
<style>
td { font-family:arial; font-size:11px; color:#333; }
</style>
<script>
	
	$(document).ready(function() {
		var $content = $('#content');
		$content.find("table.akilliTablo").dataTable({
			"sPaginationType": "full_numbers"
		});
		$content.find(".btn-update").live("click",function(){
			var o = $(this);
			var p = o.parent().parent();
			$.ajax({
			  type: "GET",
			  url: "functions.php",
			  data: { twitterUserName: o.attr("username")},
			  dataType: "json",
			  success:function(data){
				$(".v-follower",p).html(data.Twitter.followers_count),
				$(".v-following",p).html(data.Twitter.friends_count),
				$(".v-influence",p).html(data.Klout.true_reach),
				$(".v-klout",p).html(data.Klout.kscore),
				alert("Guncelleme islemi yapildi");

			  }
			});
			return false;
		});
	});
</script>
<?php
	if($_REQUEST["tID"] != "")
	{
		?>
		<table class="akilliTablo">
		<thead>
			<tr>
						<th>Foto</th> 
						<th>Username</th> 
						<th>Follower</th> 
						<th>Tur</th> 
						<th>Influence</th> 
						<th>Fiyat</th> 
			</tr>
		</thead>
		<tbody>
			<?php
			
			
				$sql = 'SELECT * FROM users WHERE type='. $_REQUEST["tID"];
			    foreach ($dbh->query($sql) as $row):
			?>

			<tr class="gradeA"> 
				<td><img src="<?php echo $row['avatar']; ?>" width="50px" height="50px" border="0" /></td> 
				<td><?php echo $row['username']; ?></td> 
				<?php
					$sqlSub = 'SELECT * FROM numbers WHERE userID='.$row['twitterid'].' LIMIT 1';
				    foreach ($dbh->query($sqlSub) as $rowSub):
				?>
				<td class="v-follower"><?php echo $rowSub['follower']; ?></td> 
				<td class=""><?php echo $row['type']; ?></td> 
				<td class="v-influence"><?php echo $rowSub['influence']; ?></td> 
				<td class="v-cost">
				<?php
				$followerCount = $rowSub['follower'];
				$influenceCount = $rowSub['influence'];	
				$cost = $influenceCount * 0.002;
					if($influenceCount < $followerCount) {
									$cost += (($followerCount - $influenceCount) * 0.001);
					}
				echo $cost;
				?>		
				</td> 
				<?php endforeach; ?>
			
			</tr>
			<?php endforeach; ?>
		</tbody>
		</table>
		<?php
	}
	else
	{
		?>
		<table class="akilliTablo">
		<thead>
			<tr>
						<th>Foto</th> 
						<th>Username</th> 
						<th>Ad</th> 
						<th>Follower</th> 
						<th>Tür</th> 
						<th>Influence</th> 
						<th>Klout</th> 
						<th>Fiyat</th> 
			</tr>
		</thead>
		<tbody>
			<?php
				$sql = 'SELECT * FROM users';
			    foreach ($dbh->query($sql) as $row):
			?>

			<tr class="gradeA"> 
				<td><img src="<?php echo $row['avatar']; ?>" width="50px" height="50px" border="0" /></td> 
				<td><?php echo $row['username']; ?></td> 
				<td><?php echo $row['name']; ?></td> 

				<?php
					$sqlSub = 'SELECT * FROM numbers WHERE userID='.$row['twitterid'].' LIMIT 1';
				    foreach ($dbh->query($sqlSub) as $rowSub):
				?>
				<td class="v-follower"><?php echo $rowSub['follower']; ?></td> 
				<td class=""><?php echo $row['type']; ?></td> 
				<td class="v-influence"><?php echo $rowSub['influence']; ?></td> 
				<td class="v-klout"><?php echo $rowSub['klout']; ?></td> 
				<td class="v-cost">
				<?php
				$followerCount = $rowSub['follower'];
				$influenceCount = $rowSub['influence'];	
				$cost = $influenceCount * 0.002;
					if($influenceCount < $followerCount) {
									$cost += (($followerCount - $influenceCount) * 0.001);
					}
				echo $cost;
				?>		
				</td> 
				<?php endforeach; ?>
			
			</tr>
			<?php endforeach; ?>
		</tbody>
		</table>
		<?php
	}

?>
