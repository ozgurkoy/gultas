<?php
ini_set('error_reporting', E_ALL);
error_reporting(-1);

if(!isset($_SESSION))
{
	session_start();
}
$script_filename = explode("/",$_SERVER["SCRIPT_NAME"]);
$script_filename=$script_filename[count($script_filename)-1];
$permissions = array();

// online
//
/*
$hostname = 'esquel.office.admingle.net';
$username = 'admingleuser';
$password = 't73undxD1mnzA83ahdc9mw3Fc';
*/

$hostname = 'localhost';
$username = 'dev0';
$password = 'Kow8U697mAv';

$dba = null;

try {
	//$dbh = new PDO("mysql:host=$hostname;dbname=admingleoffice", $username, $password);
	$dbh = new PDO("mysql:host=$hostname;dbname=office", $username, $password);
	$dbh->exec("SET NAMES UTF8");
	$dbh->exec("SET FOREIGN_KEY_CHECKS=0");

	$dba = new PDO("mysql:host=$hostname;dbname=admingleDevelopment_main", $username, $password);
	$dba->exec("SET NAMES UTF8");
	$dba->exec("SET FOREIGN_KEY_CHECKS=0");
}
catch(PDOException $e)
{
	die($e->getMessage());
}

$baseURL = "http://t.admoffice.com/";

function checkPermission($p){
	global $permissions;
	if (in_array($p, $permissions))
		return true;
	else
		return false;

}

class PermissionTypes{
	const dashboard = "dashboard";
	const firms = "firms";
	const addFirm = "addFirm";
	const editFirm = "editFirm";
	const deleteFirm = "deleteFirm";
	const sectors = "sectors";
	const addSector = "addSector";
	const deleteSector = "deleteSector";
	const salesChannels = "salesChannels";
	const addSalesChannel = "addSalesChannel";
	const deleteSalesChannel = "deleteSalesChannel";
	const publishers = "publishers";
	const addPublisher = "addPublisher";
	const changeStatus = "changeStatus";
	const deleteStatus = "deleteStatus";
	const admin = "admin";
	const deletePublisher = "deletePublisher";
}

if (isset($_COOKIE["admCRM_LU"]) && isset($_COOKIE["admCRM_LK"])){

	$user = $_COOKIE["admCRM_LU"];
	$key = $_COOKIE["admCRM_LK"];

	$sqlC = "SELECT id, password, salt FROM members WHERE username='$user' AND password='$key'";

	$userData = $dbh->query($sqlC)->fetch();
	if ($userData){
		$_SESSION['valid'] = "Y";
		$_SESSION['userid'] = $user;
		$_SESSION['accID'] = $userData['id'];
		if ($script_filename == "login.php"){
			header('Location: index.php');
			exit;
		}
	}
}

if ($script_filename != "login.php" && $script_filename != "logincheck.php" && !(isset($_SESSION['valid']) && $_SESSION['valid'] == "Y"))
{
	//die(!(isset($_SESSION['valid']) && $_SESSION['valid'] == "Y"));
	    header('Location: login.php');
		exit;
}

if (isset($_SESSION['accID'])){
	$rows = $dbh->query("SELECT m.name FROM permissions p INNER JOIN modules m ON m.ID = p.module WHERE p.member = " . $_SESSION['accID'])->fetchAll();
	foreach ($rows as $row){
		$permissions[] = $row["name"];
	}
	unset($rows);
}
//die(print_r($permissions));
?>
