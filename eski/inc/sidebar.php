	<ul id="nav">
		<li class="i_house"><a href="<?php echo $baseURL; ?>index.php"><span>Ana Sayfa</span></a></li>
			<?php if (checkPermission(PermissionTypes::firms)): ?>
			<li class="i_house"><a><span>Firmalar</span></a>
				<ul>
					<li><a href="<?php echo $baseURL; ?>allFirms.php"><span>Tüm firmalar</span></a></li>
					<li><a href="<?php echo $baseURL; ?>statuses.php"><span>Firma Durumları</span></a></li>
					<li><a href="<?php echo $baseURL; ?>notes.php"><span>Firma Notları</span></a></li>

				</ul>
			</li>
			<?php endif; ?>
			<?php if (checkPermission(PermissionTypes::sectors)): ?>
			<li class="i_money"><a><span>Sektörler</span></a>
				<ul>
					<?php
						$sql = 'SELECT * FROM sector';
					    foreach ($dbh->query($sql) as $row):
					?>
					<li><a href="<?php echo $baseURL; ?>firms.php?tID=<?php echo $row['ID']; ?>&v=s"><span><?php echo $row['name']; ?></span></a></li>
					<?php endforeach; ?>
				</ul>
			</li>
			<?php endif; ?>
			<?php if (checkPermission(PermissionTypes::salesChannels)): ?>
			<li class="i_money"><a><span>Satış Kanalları</span></a>
				<ul>
					<?php
						$sql = 'SELECT * FROM customertype ORDER BY name ASC';
					    foreach ($dbh->query($sql) as $row):
					?>
					<li><a href="<?php echo $baseURL; ?>firms.php?tID=<?php echo $row['ID']; ?>&v=t"><span><?php echo $row['name']; ?></span></a></li>
					<?php endforeach; ?>
				</ul>
			</li>
			<?php endif; ?>
			<?php if (checkPermission(PermissionTypes::publishers)): ?>
			<li class="i_house"><a><span>Yayıncılar</span></a>
				<ul>
					<li><a href="<?php echo $baseURL; ?>allHandles.php"><span>Tüm Yayıncılar</span></a></li>

					<?php
						$sql = 'SELECT * FROM userType';
						foreach ($dbh->query($sql) as $row):
					?>
					<li><a href="<?php echo $baseURL; ?>lists.php?tID=<?php echo $row['ID']; ?>"><span><?php echo $row['type']; ?></span></a></li>
					<?php endforeach; ?>
				</ul>
			</li>
			<?php endif; ?>


		<li class="i_create_write"><a href="<?php echo $baseURL; ?>logout.php"><span>Çıkış</span></a></li>
	</ul>