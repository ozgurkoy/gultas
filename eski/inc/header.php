<div id="logo">
	<a href="index.php">Logo Here</a>
</div>
<div id="header">
	<ul id="headernav">
		<li><ul>
			<li><a href="#">Yönetim</a>
				<ul>
					<?php if (checkPermission(PermissionTypes::addFirm)): ?>
					<li><a href="admin_addFirm.php">Yeni Firma Ekle</a></li>
					<?php endif; ?>
					<?php if (checkPermission(PermissionTypes::addSector)): ?>
					<li><a href="admin_addSector.php">Yeni Sektör Ekle</a></li>
					<?php endif; ?>
					<?php if (checkPermission(PermissionTypes::admin)): ?>
					<li><a href="admin_addFirmType.php">Yeni Firma Tipi Ekle</a></li>
					<?php endif; ?>
					<?php if (checkPermission(PermissionTypes::addPublisher)): ?>
					<li><a href="admin_addHandle.php">Yeni Twitter Kullanıcısı</a></li>
					<?php endif; ?>
					<?php if (checkPermission(PermissionTypes::admin)): ?>
					<li><a href="admin_permissions.php">Yetkiler</a></li>
					<?php endif; ?>

				</ul>
			</li>
		</ul></li>
	</ul>

</div>