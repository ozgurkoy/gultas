<?php
	include("inc/config/config.php");
	if (!checkPermission(PermissionTypes::publishers)) header('Location: index.php');
?>

<script>

	$(document).ready(function() {
		var $content = $('#content');
		$content.find("table.akilliTablo").dataTable({
			"sPaginationType": "full_numbers"
		});
		$content.find(".btn-update").live("click",function(){
			var o = $(this);
			var p = o.parent().parent();
			$.ajax({
			  type: "GET",
			  url: "functions.php",
			  data: { twitterUserName: o.attr("username")},
			  dataType: "json",
			  success:function(data){
				$(".v-follower",p).html(data.Twitter.followers_count),
				$(".v-following",p).html(data.Twitter.friends_count),
				$(".v-influence",p).html(data.Klout.true_reach),
				$(".v-klout",p).html(data.Klout.kscore),
				$(".v-cost",p).html(data.Cost),
				alert("Guncelleme islemi yapildi");

			  }
			});
			return false;
		});
	});
</script>
		<table class="akilliTablo">
		<thead>
			<tr>
				<th>Foto</th>
				<th>Username</th>
				<th>Ad</th>
				<th>Follower</th>
				<th>Tür</th>
				<th>Influence</th>
				<th>Klout</th>
				<th>Fiyat</th>
				<th>&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			<?php
				if($_REQUEST["tID"] != "")
					$sql = 'SELECT * FROM users WHERE type='. $_REQUEST["tID"];
				else
					$sql = 'SELECT * FROM users';
			    foreach ($dbh->query($sql) as $row):
			?>

			<tr class="gradeA">
				<td><img src="<?php echo $row['avatar']; ?>" width="50px" height="50px" border="0" /></td>
				<td><?php echo $row['username']; ?></td>
				<td><?php echo $row['name']; ?></td>

				<?php
					$sqlSub = 'SELECT * FROM numbers WHERE userID='.$row['twitterid'].' LIMIT 1';
				    foreach ($dbh->query($sqlSub) as $rowSub):
				?>
				<td class="v-follower"><?php echo $rowSub['follower']; ?></td>
				<td class=""><?php echo $row['type']; ?></td>
				<td class="v-influence"><?php echo $rowSub['influence']; ?></td>
				<td class="v-klout"><?php echo $rowSub['klout']; ?></td>
				<td class="v-cost">
				<?php
				$followerCount = $rowSub['follower'];
				$influenceCount = $rowSub['influence'];
				$cost = $influenceCount * 0.002;
				if($influenceCount < $followerCount) {
					$cost += (($followerCount - $influenceCount) * 0.001);
				}
				echo $cost;
				?>
				</td>
				<?php endforeach; ?>
				<td>

					<?php if (checkPermission(PermissionTypes::addPublisher)): ?>
					<a href="functions.php?update=1&twitterUserName=<?php echo $row['username']; ?>" class="btn-update btn i_power yellow" title="Güncelle" username="<?php echo $row['username']; ?>"></a>
					<?php endif; ?>
					<?php if (checkPermission(PermissionTypes::deletePublisher)): ?>
					<a href="delete.php?twitterUserName=<?php echo $row['username']; ?>&id=<?php echo $row['twitterid']; ?>" class="btn-del  btn i_trashcan red" title="Sil" username="<?php echo $row['username']; ?>"></a>
					<?php endif; ?>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
		</table>
