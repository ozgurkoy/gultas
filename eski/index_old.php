<?php
include("inc/config/config.php");
?>
<!doctype html>
<html lang="tr-tr">
<head>
	<?php include("inc/meta.php");?>
</head>
<body>
			<?php include("inc/headnav.php");?>
		<header>
			<?php include("inc/header.php"); ?>
		</header>
		<nav>
			<?php include("inc/sidebar.php"); ?>
		</nav>

		<section id="content">
			<?php if (checkPermission(PermissionTypes::dashboard)): ?>
			<div class="g6">
				<h3>Durum Dağılımı</h3>
				<p>Durumlara göre dağılımlar</p>
				<table class="chart" data-type="pie">
					<thead>
						<tr>
							<th></th>
							<?php
								$sql = "
									SELECT
										st.type,
										count(*) cnt
									FROM status s
										INNER JOIN statusType st on st.ID = s.status
									GROUP BY
										st.type
									ORDER BY
										st.type ASC
								";
								foreach ($dbh->query($sql) as $row):
							?>
							<th><?php echo $row['type']; ?></th>
							<?php endforeach; ?>
						</tr>
					</thead>
					<tbody>
						<?php
							foreach ($dbh->query($sql) as $row):
						?>
						<tr>
							<th><?php echo $row['type']; ?></th>
							<td><?php echo $row['cnt']; ?></td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
			<div class="g6">
				<h3>Tamamlanmış Proje Dağılımı</h3>
				<p>Satışçılara göre tamamlanmış proje dağılımları.</p>
				<table class="chart" data-type="pie" data-tilt="0.5">
					<thead>
						<tr>
							<th></th>
							<th>Semra Süzgün</th>
							<th>TBD</th>
							<th>TBD</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th>Semra Süzgün</th>
							<td>48</td>
						</tr>
						<tr>
							<th>TBD</th>
							<td>8</td>
						</tr>
						<tr>
							<th>TBD 2</th>
							<td>28</td>
						</tr>
						<tr>
							<th>TBD 3</th>
							<td>38</td>
						</tr>
					</tbody>
				</table>
			</div>

			<div class="g12">
				<hr>
				<h3>Satış Performans Grafiği</h3>
				<table class="chart">
					<thead>
						<tr>
							<th></th>
							<?php
								$sql = "
									SELECT `date`
									FROM status s
									WHERE s.status = 6
									GROUP BY `date`
									ORDER BY `date`
								";
								foreach ($dbh->query($sql) as $row):
							?>
							<th><?php echo $row['date']; ?></th>
							<?php endforeach; ?>

						</tr>
					</thead>
					<?php
						$sql = "
							SELECT
								CONCAT_WS(' ',m.name, m.surname) manager_name,
								d.`date`,
								(SELECT count(*) FROM status ss WHERE ss.manager = m.ID AND ss.date = d.date AND ss.status = s.status) cnt
							FROM (
									SELECT `date`
									FROM status s
									WHERE s.status = 6 AND `date` > '" . date('Y-m-d', strtotime("-1 month")) . "'
									GROUP BY `date`
								) d
								CROSS JOIN status s
								LEFT JOIN members m ON m.ID = s.manager
							WHERE
								s.status = 6
								AND s.`date` > '" . date('Y-m-d', strtotime("-1 month")) . "'
							GROUP BY
								CONCAT_WS(' ',m.name, m.surname),
								d.`date`
							ORDER BY
								CONCAT_WS(' ',m.name, m.surname),
								s.date
						";
						$members = array();
						foreach ($dbh->query($sql) as $row){
							if (!isset($members[$row['manager_name']])){
								$members[$row['manager_name']] = array();
							}
							$members[$row['manager_name']][] = $row["cnt"];
						}
					?>
					<tbody>
						<?php
						foreach($members as $k=>$v):
						?>
						<tr>
							<th><?php echo $k; ?></th>
							<?php
							foreach($v as $vv):
							?>
							<td><?php echo $vv; ?></td>
							<?php endforeach; ?>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>

				<hr>
			</div>
			<?php else: ?>
				<script>location.href='notes.php'</script>
			<?php endif; ?>
		</section>

		<footer><?php include("inc/footer.php"); ?></footer>
</body>
</html>