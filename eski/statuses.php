<?php
include("inc/config/config.php");
if (!checkPermission(PermissionTypes::firms)) header('Location: index.php');
?>
<!doctype html>
<html lang="tr-tr">
<head>
	<?php include("inc/meta.php");?>
	<script type="text/javascript">
		$(document).ready(function(){
			$(".btn-status-delete").live("click",function(){
				if (confirm("Durumu silmek istediğinize emin misiniz?")){
					var o = $(this);
					$.get("customerFunctions.php",{del : "3", statusID : o.attr("statusID")},function(data){
						if (data.error == 0){
							o.parent().parent().fadeOut();
						}
					},"json");
				}
				return false;
			});
		});
	</script>
</head>
<body>
			<?php include("inc/headnav.php");?>
		<header>
			<?php include("inc/header.php"); ?>
		</header>
		<nav>
			<?php include("inc/sidebar.php"); ?>
		</nav>
		<section id="content">

			<div class="g12">
				<script>

					$(document).ready(function() {
						var $content = $('#content');
						$content.find("table.dataTable").dataTable({
							"sPaginationType": "full_numbers"
						});

					});
				</script>
							<!-- last notes -->
							<div class="widget" id="newfirm" data-icon="calendar">
								<h3 class="handle">Notlar</h3>
								<div>

										<table class="dataTable">
											<thead>
												<tr>
													<th width="90">Tarih</th>
													<th width="90">Durum</th>
													<th width="120">İlgili Firma</th>
													<th width="120">İlgili Kampanya</th>
													<th>Not</th>
													<th width="90">Dosya</th>
													<th width="90">Duzenleyen</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<?php
													$sql = "
														SELECT
															s.ID,
															s.date,
															s.note,
															s.file,
															s.manager,
															s.campaign,
															st.type AS statusType,
															CONCAT_WS(' ',m.name,m.surname) AS manager_name,
															c.name AS company_name,
															c.ID AS company_id,
															m.username AS manager_username
														FROM `status` s
															INNER JOIN statusType st ON st.ID = s.status
															INNER JOIN members m ON m.ID = s.manager
															INNER JOIN customers c ON c.ID = s.company
														ORDER BY date DESC
													";
													foreach ($dbh->query($sql) as $row):
												?>

												<tr class="gradeA">

													<td><?php echo $row['date']; ?></td>
													<td><?php echo $row['statusType']; ?></td>
													<td><a href="<?php echo $row['company_id']; ?>.adm"><?php echo $row['company_name']; ?></a></td>
													<td>
													<?php
														if (strlen($row["campaign"])>0 && $row["campaign"] != 0){
														$sqlx = "SELECT CONCAT_WS(' ',campCode,title) AS title FROM campaign WHERE id=" . $row["campaign"];

														$rowc = $dba->query($sqlx)->fetch();
															echo $rowc["title"];
														}else{
															echo "-";
														}
													?>
													</td>
													<td><?php echo $row['note']; ?></td>
													<td><a href="./upload/<?php echo $row['file']; ?>"><?php echo $row['file']; ?></a></td>
													<td><a href="/username/<?php echo $row['manager_username']; ?>"><?php echo $row['manager_name']; ?></a></td>
													<td>
														<?php if (checkPermission(PermissionTypes::deleteStatus) && $row["manager"] == $_SESSION["accID"]): ?>
														<a href="#" class="btn-status-delete btn i_trashcan red" title="Sil" statusID="<?php echo $row['ID']; ?>"></a>
														<?php endif; ?>
													</td>
												</tr>
												<?php endforeach; ?>
											</tbody>
											</table>


								</div>
							</div>
							<!-- end -->


			</div>

		</section>
		<footer><?php include("inc/footer.php"); ?></footer>
</body>
</html>