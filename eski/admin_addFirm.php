<?php
include("inc/config/config.php");
if (!checkPermission(PermissionTypes::addFirm)) header('Location: index.php');
?>
<!doctype html>
<html lang="tr-tr">
<head>
	<?php include("inc/meta.php");?>
</head>
<body>
			<?php include("inc/headnav.php");?>
		<header>
			<?php include("inc/header.php"); ?>
		</header>
		<nav>
			<?php include("inc/sidebar.php"); ?>
		</nav>
		<section id="content">
			<div class="g12 widgets">




				<div class="widget" id="newfirm" data-icon="calendar">
					<h3 class="handle">Yeni Firma Ekle</h3>
					<div>
						<form action="customerFunctions.php?add=3" method="post" id="formFirmAdd"  data-ajax="false">
							<fieldset>
								<section><label for="text_field">Firma :</label>
									<div><input type="text" id="text_field" name="name"></div>
								</section>
								<section><label for="text_field">Yetkili Kişi :</label>
									<div><input type="text" id="text_field" name="responsiblehead"></div>
								</section>
								<section><label for="text_field">Adres :</label>
									<div><input type="text" id="text_field" placeholder="Haydar Aliyev Caddesi, Çıra Sokak, Haydar Bey Apt. No:2 D:8 Tarabya Sarıyer" name="location"></div>
								</section>
								<section>
									<label for="customer_type">Şehir</label>
									<div>
										<select name="city" id="cities">
											<option/>Seçin</option>
											<?php
												$sqlx = 'SELECT * FROM cities ORDER BY name ASC';
											    foreach ($dbh->query($sqlx) as $rowx):
											?>
												<option value="<?php echo $rowx['ID']; ?>" /><?php echo $rowx['name']; ?></option>
												<?php endforeach; ?>
										</select>
									</div>
								</section>
								<section><label for="text_field">Telefon :</label>
									<div><input type="text" placeholder="902122232323" id="text_field" name="phone"></div>
								</section>
								<section><label for="text_field">GSM :</label>
									<div><input type="text" placeholder="905075075071" id="text_field" name="gsm"></div>
								</section>
									<section>
										<label for="customer_sector">Sektör</label>
										<div>
											<select name="sector" id="sectorsFirm">
												<option/>Seçin</option>
												<?php
													$sqlx = 'SELECT * FROM sector  ORDER BY name ASC';
												    foreach ($dbh->query($sqlx) as $rowx):
												?>
													<option value="<?php echo $rowx['ID']; ?>" /><?php echo $rowx['name']; ?></option>
													<?php endforeach; ?>
											</select>
										</div>
									</section>
									<section>
										<label for="customer_type">Tip</label>
										<div>
											<select name="type" id="typesFirm">
												<option/>Seçin</option>
												<?php
													$sqlx = 'SELECT * FROM customertype  ORDER BY name ASC';
												    foreach ($dbh->query($sqlx) as $rowx):
												?>
													<option value="<?php echo $rowx['ID']; ?>" /><?php echo $rowx['name']; ?></option>
													<?php endforeach; ?>
											</select>
										</div>
									</section>
								<section>
									<div><button class="newCustomerType submit" name="submitbuttonname" value="submitbuttonvalue">Ekle</button></div>
								</section>
							</fieldset>
						</form>
					</div>
				</div>


			</div>

		</section>
		<footer><?php include("inc/footer.php"); ?></footer>
</body>
</html>