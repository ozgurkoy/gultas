<?php
	include("inc/config/config.php");
	if (!checkPermission(PermissionTypes::firms)) die("Bu işlem içiniz yetkiniz bulunmamaktadır.");
?>

<script>

	$(document).ready(function() {
		var $content = $('#content');
		$content.find("table.akilliTablo").dataTable({
			"sPaginationType": "full_numbers"
		});

	});
</script>
<?php
	{
		?>
		<table class="akilliTablo">
		<thead>
			<tr>
				<th>Firma</th>
				<th>Yetkili</th>
				<th>Lokasyon</th>
				<th>Şehir</th>
				<th>Telefon</th>
				<th>&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			<?php
				if($_REQUEST["v"] == "s")
					$sql = 'SELECT c.*, ci.name as city FROM customers c LEFT JOIN cities ci ON c.city = ci.ID WHERE c.sectorID='. $_REQUEST["tID"];
				else if($_REQUEST["v"] == "t")
					$sql = 'SELECT c.*, ci.name as city  FROM customers c LEFT JOIN cities ci ON c.city = ci.ID WHERE c.type='. $_REQUEST["tID"];
				else
					$sql = 'SELECT c.*, ci.name as city  FROM customers c LEFT JOIN cities ci ON c.city = ci.ID';
			    foreach ($dbh->query($sql) as $row):
			?>

			<tr class="gradeA">
				<td><a href="<?php echo $row['ID']; ?>.adm"><?php echo $row['name']; ?></a></td>
				<td><?php echo $row['responsiblehead']; ?></td>
				<td><?php echo $row['location']; ?></td>
				<td><?php echo $row['city']; ?></td>
				<td><?php echo $row['phone']; ?></td>
				<td>
					<?php if (checkPermission(PermissionTypes::editFirm)): ?>
						<a href="admin_editFirm.php?firmID=<?php echo $row['ID']; ?>" class="btn i_pencil" title="Güncelle"></a>
					<?php endif; ?>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
		</table>
		<?php
	}

?>
