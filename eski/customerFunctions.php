<?php
include("inc/config/config.php");
$time = time();
$type = $_REQUEST["type"];
	if(isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
		//AJAX Request
		// firma notu silme
		if($_REQUEST["del"] == "2")
		{
			if (!checkPermission(PermissionTypes::editFirm)) header('Location: index.php');
			$delUser = $dbh->exec("DELETE FROM `notes` WHERE noted = (SELECT username FROM members WHERE id=" . $_SESSION["accID"] . ") AND ID=".$_REQUEST["noteID"]);
			echo json_encode(array("error"=>0));
		}
		else if($_REQUEST["del"] == "3")
		{
			if (!checkPermission(PermissionTypes::deleteStatus)) header('Location: index.php');
			$delUser = $dbh->exec("DELETE FROM `status` WHERE manager=".$_SESSION["accID"] ." AND ID=".$_REQUEST["statusID"]);
			echo json_encode(array("error"=>0,"sql"=>"DELETE FROM `status` WHERE manager=".$_SESSION["accID"] ." AND ID=".$_REQUEST["statusID"]));
		}
	}else{
		//Native Form Submit
		$name = $_POST['name'];

		// sektör ekleme
		if($_REQUEST["add"] == "1")
		{
			if (!checkPermission(PermissionTypes::addSector)) header('Location: index.php');
			$sql = "SELECT COUNT(*) FROM `sector` WHERE name='".$_REQUEST["name"]."'";
			if ($res = $dbh->query($sql)) {
			  if ($res->fetchColumn() > 0) {
							echo "<script>location.href='admin_addSector.php?kayit=Mevcut'</script>";
			    }
			  else {
					$addDBuser = $dbh->exec("INSERT INTO `sector`(`name`) VALUES ('$name')");
					echo "<script>location.href='admin_addSector.php'</script>";
			    }
			}
		}

		// firma tipi ekleme
		if($_REQUEST["add"] == "2")
		{
			if (!checkPermission(PermissionTypes::addFirm)) header('Location: index.php');
			if ($res = $dbh->query($sql)) {
			  if ($res->fetchColumn() > 0) {
							echo "<script>location.href='admin_addFirmType.php?kayit=Mevcut'</script>";
			    }
			  else {
					$addDBuser = $dbh->exec("INSERT INTO `customertype`(`name`) VALUES ('$name')");
					echo "<script>location.href='admin_addFirmType.php'</script>";
			    }
			}
		}

		// firma ekleme
		if($_REQUEST["add"] == "3")
		{
			if (!checkPermission(PermissionTypes::addFirm)) header('Location: index.php');
			$sql = "SELECT COUNT(*) FROM `customers` WHERE name='".$_REQUEST["name"]."'";
			if ($res = $dbh->query($sql)) {
			  if ($res->fetchColumn() > 0) {
						echo "<script>location.href='admin_addFirm.php?kayit=Mevcut'</script>";
			    }
			  else {

					$quotes = array('/"/',"/'/");
					$replacements = array('%22','%27');

					$location = $_POST['location'];
					$phone = $_POST['phone'];
					$gsm = $_POST['gsm'];
					$sector = $_POST['sector'];
					$type = $_POST['type'];
					$city = $_POST['city'];
					$responsiblehead = $_POST['responsiblehead'];

					$addDBuser = $dbh->exec("INSERT INTO `customers`(`name`,`responsiblehead`,`location`,`city`,`phone`,`gsm`,`type`,`sectorID`) VALUES ('$name','$responsiblehead','$location','$city','$phone','$gsm','$type','$sector')");
					echo "<script>location.href='admin_addFirm.php'</script>";

			    }
			}
		}

		// firma güncelleme
		if($_REQUEST["add"] == "4")
		{
			if (!checkPermission(PermissionTypes::editFirm)) header('Location: index.php');

			$companyID = $_POST['firmID'];
			$location = $_POST['location'];
			$phone = $_POST['phone'];
			$gsm = $_POST['gsm'];
			$sector = $_POST['sector'];
			$type = $_POST['type'];
			$city = $_POST['city'];
			$responsiblehead = $_POST['responsiblehead'];
			$updateSQL = "UPDATE `customers` SET name='$name', responsiblehead='$responsiblehead', location='$location', city='$city', phone='$phone', gsm='$gsm', type='$type', sectorID='$sector' WHERE ID=". $_REQUEST["id"];
			//print($updateSQL);
			$addDBuser = $dbh->exec($updateSQL);
			echo "<script>location.href='firmDetails.php?firmID=".$companyID."'</script>";
		}

		// firmaya not ekleme
		if($_REQUEST["add"] == "5")
		{
			if (!checkPermission(PermissionTypes::editFirm)) header('Location: index.php');

			$note = $_POST['note'];
			$noted = $_SESSION['userid'];
			$companyID = $_POST['firmID'];


			$updateSQL = "INSERT INTO `notes`(`companyID`,`note`,`date`,`noted`) VALUES ('$companyID','$note','$time','$noted')";
			//print($updateSQL);
			$addNote = $dbh->exec($updateSQL);
			echo "<script>location.href='firmDetails.php?firmID=".$companyID."'</script>";
		}

		// firmaya durum ekleme
		if($_REQUEST["add"] == "6")
		{
			if (!checkPermission(PermissionTypes::editFirm)) header('Location: index.php');

			require_once 'upload.php';
			$upload_handler = new UploadHandler();

			$info = $upload_handler->post();
			//die(print_r($info));
			$note = $_POST['note'];
			$date = $_POST['date'];
			$manager = $_POST['manager'];
			$company = $_POST['company'];
			$status = $_POST['status'];
			$campaign = $_POST['campaign'];

			$updateSQL = "INSERT INTO `status`(`status`, `company`,`date`,`manager`,`note`,`file`,`campaign`) VALUES ('$status','$company','$date','$manager','$note','" . (isset($info[0]->name) && strlen($info[0]->name)>0 ? $info[0]->name : "") . "','$campaign')";
			//die($updateSQL);
			$addNote = $dbh->exec($updateSQL);
			echo "<script>location.href='firmDetails.php?firmID=".$company."'</script>";
		}

		// kisi ekleme
		if($_REQUEST["add"] == "7")
		{
			if (!checkPermission(PermissionTypes::addFirm)) header('Location: index.php');
			$sql = "SELECT COUNT(*) FROM `contacts` WHERE email='".$_REQUEST["email"]."'";
			if ($res = $dbh->query($sql)) {
			  if ($res->fetchColumn() > 0) {
						echo "<script>location.href='admin_addContact.php?kayit=Mevcut'</script>";
			    }
			  else {

			  	require_once 'upload.php';
				$upload_handler = new UploadHandler();

				$info = $upload_handler->post();
					$quotes = array('/"/',"/'/");
					$replacements = array('%22','%27');

					$name = $_POST['name'];
					$surname = $_POST['surname'];
					$account = $_POST['account'];
					$email = $_POST['email'];
					$phone = $_POST['phone'];
					$mobile = $_POST['mobile'];
					$assistant = $_POST['assistant'];
					$assistantPhone = $_POST['assistantPhone'];
					$title = $_POST['title'];
					$department = $_POST['department'];
					$dateOfBirth = $_POST['dateOfBirth'];
					$skype = $_POST['skype'];
					$gtalk = $_POST['gtalk'];
					$linkedin = $_POST['linkedin'];
					$facebook = $_POST['facebook'];
					$twitter = $_POST['twitter'];
					$address = $_POST['address'];
					$city = $_POST['city'];
					$country = $_POST['country'];


					$addDBuser = $dbh->exec("INSERT INTO `contacts`(`name`,`surname`,`account`,`email`,`phone`,`mobile`,`assistant`,`assistantPhone`,`title`,`department`,`dateOfBirth`,`skype`,`gtalk`,`linkedin`,`facebook`,`twitter`,`address`,`city`,`country`) VALUES ('$name','$surname','$account','$email','$phone','$mobile','$assistant','$assistantPhone','$title','$department','$dateOfBirth','$skype','$gtalk','$linkedin','$facebook','$twitter','$address','$city','$country')");
					echo "<script>location.href='admin_addContact.php'</script>";

			    }
			}
		}

		// firma silme
		if($_REQUEST["del"] == "1")
		{
			if (!checkPermission(PermissionTypes::deleteFirm)) header('Location: index.php');
			$delUser = $dbh->exec("DELETE FROM `customers` WHERE ID='".$_REQUEST["firmID"]."'");
			echo "<script>location.href='allFirms.php'</script>";
		}
	}

?>
