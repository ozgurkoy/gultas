<?php
include("inc/config/config.php");
if (!checkPermission(PermissionTypes::firms)) header('Location: index.php');
?>
<!doctype html>
<html lang="tr-tr">
<head>
	<?php include("inc/meta.php");?>
	<script type="text/javascript">
		$(document).ready(function(){
			$(".i_trashcan").click(function(){
				if (!confirm("Firmayı silmek istediğinize emin misiniz?"))
					return false;
			});
		});
	</script>
</head>
<body>
			<?php include("inc/headnav.php");?>
		<header>
			<?php include("inc/header.php"); ?>
		</header>
		<nav>
			<?php include("inc/sidebar.php"); ?>
		</nav>
		<section id="content">
<div class="g12 widgets">
	<script>

		$(document).ready(function() {
			var $content = $('#content');
			$content.find("table.akilliTablo").dataTable({
				"sPaginationType": "full_numbers"
			});

		});
	</script>
					<div class="widget" data-icon="number" id="widget_ajax">
						<h3 class="handle">Firmalar</h3>
						<div>
							<table class="akilliTablo">
							<thead>
								<tr>
									<th>Firma</th>
									<th>Yetkili</th>
									<th>Lokasyon</th>
									<th>Şehir</th>
									<th>Telefon</th>
									<th>&nbsp;</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$sql = 'SELECT c.*, ci.name as city  FROM customers c LEFT JOIN cities ci ON c.city = ci.ID';
								    foreach ($dbh->query($sql) as $row):
								?>
								<tr class="gradeA">
									<td><a href="<?php echo $row['ID']; ?>.adm"><?php echo $row['name']; ?></a></td>
									<td><?php echo $row['responsiblehead']; ?></td>
									<td><?php echo $row['location']; ?></td>
									<td><?php echo $row['city']; ?></td>
									<td><?php echo $row['phone']; ?></td>
									<td>
										<?php if (checkPermission(PermissionTypes::editFirm)): ?>
										<a href="admin_editFirm.php?firmID=<?php echo $row['ID']; ?>" class="btn i_pencil yellow" title="Güncelle"></a>
										<?php endif; ?>
										<?php if (checkPermission(PermissionTypes::deleteFirm)): ?>
										<a href="customerFunctions.php?del=1&firmID=<?php echo $row['ID']; ?>" class="btn i_trashcan red" title="Sil"></a>
										<?php endif; ?>
									</td>
								</tr>
								<?php endforeach; ?>
							</tbody>
							</table>
						</div>
					</div>
			</div>

		</section>
		<footer><?php include("inc/footer.php"); ?></footer>
</body>
</html>