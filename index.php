<?php

// Use the Fat-Free Framework
$main=require_once __DIR__.'/lib/base.php';

F3::config(__DIR__.'/inc/config/settings.cfg');

if ($main->get('SESSION.accID')){
	$main->route('GET /','Crm->show');
	$main->route('GET /logOut','Crm->logOut');
	$main->route('GET /editPermissions','CrmUser->editPermissions');
	$main->route('POST /getUserPermissions','CrmUser->getUserPermissions');
        $main->route('POST /getUserGroups','CrmUser->getUserGroups');
        $main->route('GET /getUserGroups','CrmUser->getUserGroups');
	$main->route('POST /updateUserPermissions','CrmUser->updateUserPermissions');
        $main->route('POST /updateUserGrops','CrmUser->updateUserGrops');
	$main->route('GET /allNotes','CrmCustomer->allNotes');
	$main->route('GET /allCustomers/@ctype/@tid','CrmCustomer->allCustomers');
	$main->route('GET /allStatuses','CrmCustomer->allStatuses');
	$main->route('GET /customerDetail/@cid','CrmCustomer->customerDetail');
	$main->route('GET /addCustomer/@cid','CrmCustomer->addCustomer');
	$main->route('GET /editCustomer/@cid','CrmCustomer->addCustomer');
	$main->route('POST /saveCustomer','CrmCustomer->saveCustomer');
	$main->route('POST /deleteCustomer','CrmCustomer->deleteCustomer');
	$main->route('POST /addStatus','CrmCustomer->addStatus');
	$main->route('POST /addNote','CrmCustomer->addNote');
	$main->route('POST /deleteStatus','CrmCustomer->deleteStatus');
	$main->route('POST /deleteNote','CrmCustomer->deleteNote');
	$main->route('GET /addSector','CrmCustomer->addSector');
	$main->route('POST /saveSector','CrmCustomer->saveSector');
	$main->route('GET /addCustomerType','CrmCustomer->addCustomerType');
	$main->route('POST /saveCustomerType','CrmCustomer->saveCustomerType');
	$main->route('GET /addPublisher','CrmPublisher->addPublisher');
        $main->route('GET /addBulkPublisher','CrmPublisher->addBulkPublisher');
        
        $main->route('GET /Lists','CrmPublisherSettings->Lists');
        $main->route('POST /List','CrmPublisherSettings->ListManage');
        $main->route('GET /Tags','CrmPublisherSettings->Tags');
        $main->route('POST /updateUserTags','CrmPublisherSettings->updateUserTags');
        $main->route('POST /SaveTag','CrmPublisherSettings->TagSave');
        $main->route('GET /UserLog','CrmPublisherSettings->UserLog');
        $main->route('GET /PublisherReview','CrmPublisher->PublisherPreview');
        
        
        $main->route('GET /allPublishers','CrmPublisher->allPublishersGroup');
        $main->route('POST /admPpdateGroup','CrmPublisher->admPpdateGroup');
        $main->route('GET /allPublishers/@tid','CrmPublisher->allPublishers');        
        $main->route('GET /SearchPublishers','CrmPublisher->SearchPublishers');
        $main->route('POST /SearchPublishers','CrmPublisher->doSearchPublishers');
	$main->route('POST /savePublisher','CrmPublisher->savePublisher');
	$main->route('POST /deletePublisher','CrmPublisher->deletePublisher');
	$main->route('GET /profile','CrmUser->profile');
	$main->route('POST /saveProfile','CrmUser->saveProfile');
        $main->route('POST /adminNewPassSet','CrmUser->adminNewPassSet');
        $main->route('POST /adminUpdateProfile','CrmUser->adminUpdateProfile');
	$main->route('GET /allContacts','CrmContact->allContacts');
	$main->route('GET /addContact','CrmContact->addContacts');
	$main->route('GET /editContact/@cid','CrmContact->addContacts');
	$main->route('POST /saveContact','CrmContact->saveContact');
	$main->route('GET /contactDetail/@cid','CrmContact->contactDetail');
	$main->route('POST /deleteContact','CrmContact->deleteContact');
	$main->route('GET /mf','Crm->markafoni');
	$main->route('GET /mfu','Crm->markafoniUpdate');
	$main->route('GET /addProduct','CrmProduct->addProduct');
	$main->route('GET /editProduct/@pid','CrmProduct->addProduct');
	$main->route('POST /saveProduct','CrmProduct->saveProduct');
	$main->route('GET /allProducts','CrmProduct->allProducts');
	$main->route('POST /deleteProduct','CrmProduct->deleteProduct');
	$main->route('GET /cashFlow','CrmCashFlow->allCashFlows');
	$main->route('GET /addCashFlow','CrmCashFlow->addCashFlow');
	$main->route('POST /getCompanyBrands','CrmCashFlow->getCompanyBrands');
	$main->route('POST /getBrandCampaigns','CrmCashFlow->getBrandCampaigns');
	$main->route('GET /editCashFlow/@cid','CrmCashFlow->addCashFlow');
	$main->route('POST /saveCashFlow','CrmCashFlow->saveCashFlow');
        $main->route('GET *','Crm->error404');
} else {
	if (PHP_SAPI == 'cli'){
		F3::set('argv',$argv);
		
		require_once __DIR__.'/app/crmConsole.php';
		$main->route('GET *','CrmConsole->init');
	} else {
		$main->route('GET /','Crm->login');
		$main->route('POST /doLogin','Crm->doLogin');
		$main->route('POST /ws/addPublisherScore','CrmPublisher->addPublisherScore');
		$main->route('GET *','Crm->error404');
	}
}


F3::set('checkPermission',
	function($p) {
		return Crm::checkPermission($p);
	}
);

// Execute application
$main->run();